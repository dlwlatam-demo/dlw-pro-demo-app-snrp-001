package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter @Setter
public class TaAppsunarpLogBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String ipAddress;
	private Date createdAt;
	private String idUsuarioCrea;
	private String userKeyId;
	private String wsMethod;
	private String jsonRequest;
}

