package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.PropietarioBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PropietarioVehiculoRowMapper implements RowMapper<PropietarioBean> {

    @Override
    public PropietarioBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return PropietarioBean.builder()
                .titular(rs.getString("titular"))
                .ts_ult_sync(rs.getString("ts_ult_sync"))
                .build();
    }
}
