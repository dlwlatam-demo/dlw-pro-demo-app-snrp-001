package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UbigeoBean {
    private String dpto;
    private String prov;
    private String dist;
}
