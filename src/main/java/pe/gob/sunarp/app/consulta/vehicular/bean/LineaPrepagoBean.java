package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter 
@Setter
@AllArgsConstructor
public class LineaPrepagoBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private long lineaPrepagoId;
	private String estado;
	private String fgDeposito;
	private BigDecimal saldo;
	private String usrUltModif;
	
	private BigDecimal cuentaId;
	private BigDecimal peJuriId;
	private BigDecimal juriPersonaId;
	private BigDecimal peNatuId;
	private BigDecimal natuPersonaId;

}