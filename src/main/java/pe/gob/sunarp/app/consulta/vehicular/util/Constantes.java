package pe.gob.sunarp.app.consulta.vehicular.util;

public class Constantes {
    public static final String POR_VERIFICAR = "C";
    public static final String POR_PAGAR = "P";
    public static final String DESPACHADA = "D";
    public static final String ATENCION_PENDIENTE = "P";
    public static final String LIQUIDADO = "L";

    public static final String FLG_8 = "8";
    public static final String FLG_6 = "6";
    public static final String FLG_2 = "2";
    public static final String FLG_1 = "1";
    public static final String FLG_0 = "0";

    public static final String CERO_CERO = "0.0";

    public static final String EMISOR = "EM";
    public static final String VERIFICADOR = "VE";

    public static final String BLANK = "";
    public static final String SPACE = " ";
    public static final String N = "N";
    public static final String GENERIC_SUCCESS = "000";
    //Pattern
    public static final String BD_DATE = "yyyy-MM-dd HH:mm:ss.S";
    public static final String DET_VEH = "dd-MM-yyyy";
    public static final String TS_DATE_YMD = "yyyy-MM-dd HH:mm:ss";
    public static final String TS_DATE_DMY = "dd/MM/yyyy HH:mm:ss";

    public static final String PUBLICIDAD_COMPENDIOSA = "Publicidad Certificada";
    public static final String BOLETA_INFORMATIVA = "Boleta Informativa Vehicular";
    public static final String BUSQUEDA_NOMBRE = "B&uacute;squeda por nombre";
//    public static final String STATUS_I = "I";

    public static final int SERVICIO_BOLETA_INFORMATIVA_RPV = 219;
    //Correo
//    public static final String REGISTRADO = "[registrado]";
//    public static final String VALIDATION_CODE = "[validationCode]";

    //BD
    public static final String JDBC = "1101jdbc";
    public static final String WEBSERVICE = "WEBSERVICE";
    public static final String USER1 = "USER1";
    public static final String APPSUNARP = "APPSUNARP";
    //PKG
    public static final String WEBSERVICES_RESOURCES_BODEGA = "WEBSERVICES_RESOURCES_BODEGA";
    public static final String PKG_CONSULTAS = "PKG_CONSULTAS";
    public static final String PKG_SOLICITUD = "PKG_SOLICITUD";
    public static final String PKG_TA_USER_PAYMENT = "PKG_TA_USER_PAYMENT";
    public static final String PKG_VEHICULAR = "PKG_VEHICULAR";
    public static final String PKG_PARAMETROS = "PKG_PARAMETROS";
    public static final String PKG_TA_APPSUNARP_LOG = "PKG_TA_APPSUNARP_LOG";
    public static final String PKG_TA_USER_APP = "PKG_TA_USER_APP";
    public static final String PKG_TA_USER_TIVE = "PKG_TA_USER_TIVE";
    //SP
    public static final String GET_COUNTBYIPAPPANDDATE = "GET_COUNTBYIPAPPANDDATE";
    public static final String GET_VEHICULODATOSBODEGA = "GET_VEHICULODATOSBODEGA";
    public static final String GET_VEHICULOPROPIETARIOBODEGA = "GET_VEHICULOPROPIETARIOBODEGA";
    public static final String CTPR_REGCONSULTAAPPS = "CTPR_REGCONSULTAAPPS";
    public static final String SP_GET_VEHICULO = "SP_GET_VEHICULO";
    public static final String SP_GET_NUMPLACA_X_PARTIDA = "SP_GET_NUMPLACA_X_PARTIDA";
    public static final String SP_GET_TRANSACTION_BY_ID = "SP_GET_TRANSACTION_BY_ID";
    public static final String INS_TA_USER_PAYMENT = "INS_TA_USER_PAYMENT";
    public static final String SP_GET_VEHICULO_PARTIDA_PN = "SP_GET_VEHICULO_PARTIDA_PN";
    public static final String SP_GET_VEHICULO_PARTIDA_PJ = "SP_GET_VEHICULO_PARTIDA_PJ";
    public static final String SP_LIST_UBIGEO = "SP_LIST_UBIGEO";
    public static final String SP_LIST_GRAVAMEN = "SP_LIST_GRAVAMEN";
    public static final String SP_GET_PART_GRAV_PN = "SP_GET_PART_GRAV_PN";
    public static final String SP_GET_PART_GRAV_PJ = "SP_GET_PART_GRAV_PJ";
    public static final String SP_GET_VEHICULO_HIST = "SP_GET_VEHICULO_HIST";
    public static final String SP_GET_PLACA_ANTERIOR = "SP_GET_PLACA_ANTERIOR";
    public static final String SP_GET_TITULO_PENDIENTE = "SP_GET_TITULO_PENDIENTE";
    public static final String SP_GET_PROP_VEH_HIST_PN = "SP_GET_PROP_VEH_HIST_PN";
    public static final String SP_GET_PROP_VEH_HIST_PJ = "SP_GET_PROP_VEH_HIST_PJ";
    public static final String SP_GET_DATOS_CUENTA = "SP_GET_DATOS_CUENTA";
    public static final String SP_GET_OFIC_REG = "SP_GET_OFIC_REG";
    public static final String SP_GET_TARIFA_OFIC = "SP_GET_TARIFA_OFIC";
    public static final String SP_INSERT_TRANSACCION = "SP_INSERT_TRANSACCION";
    public static final String SP_INSERT_MOVIMIENTO = "SP_INSERT_MOVIMIENTO";
    public static final String SP_INSERT_ABONO = "SP_INSERT_ABONO";
    public static final String SP_INSERT_COMPROBANTE = "SP_INSERT_COMPROBANTE";
    public static final String SP_INS_CONSUMO = "SP_INS_CONSUMO";
    public static final String SP_UPD_LINEA_PREPAGO = "SP_UPD_LINEA_PREPAGO";
    public static final String SP_GET_LINEA_PREPAGO = "SP_GET_LINEA_PREPAGO";
    public static final String SP_NUEV_TA_APPSUNARP_LOG = "SP_NUEV_TA_APPSUNARP_LOG";
    public static final String SP_OBTN_TA_USER_APP_BY_USERKEY = "SP_OBTN_TA_USER_APP_BY_USERKEY";
    public static final String SEL_TA_USER_TIVE = "SEL_TA_USER_TIVE";
    public static final String COUNT_TA_USER_TIVE = "COUNT_TA_USER_TIVE";
    public static final String INS_TA_USER_TIVE = "INS_TA_USER_TIVE";

    //FN
    public static final String SF_GET_PLACA = "SF_GET_PLACA";
    public static final String SF_GET_TIPO_USO = "SF_GET_TIPO_USO";
    public static final String SF_GET_PERSONA_ID = "SF_GET_PERSONA_ID";

    public static final String SP_GET_SEDE = "SP_GET_SEDE";
    
    
	public static final String URL_GENERAR_BOLETA = "app.async.generar.boleta.url";
	public static final String URL_CONTEXT_PATH = "server.servlet.context-path";
	
    

}
