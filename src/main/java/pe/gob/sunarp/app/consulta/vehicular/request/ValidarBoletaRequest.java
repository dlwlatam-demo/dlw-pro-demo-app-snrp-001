package pe.gob.sunarp.app.consulta.vehicular.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidarBoletaRequest {
    private String codZona;
    private String codOficina;
    private String placa;
    private String userKeyId;
    private String usuario;
    private String correo;
    private String ipAddress;
}
