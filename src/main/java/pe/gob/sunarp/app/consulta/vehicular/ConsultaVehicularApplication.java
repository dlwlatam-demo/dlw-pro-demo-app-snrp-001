package pe.gob.sunarp.app.consulta.vehicular;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.servlet.config.annotation.AsyncSupportConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@SpringBootApplication
@EnableCaching
//@EnableResourceServer
@EnableAsync
public class ConsultaVehicularApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConsultaVehicularApplication.class, args);
	}

	@Bean
	public WebMvcConfigurer corsConfigurer() {

		return new WebMvcConfigurer() {
			@Override
			public void addCorsMappings(CorsRegistry registry) {
				registry.addMapping("/**")
						.allowedMethods("HEAD","OPTIONS")
						.allowedHeaders("Origin", "X-Requested-With", "Content-Type", "Accept")
						.allowedOrigins("http://localhost:4200")
						.allowedMethods("*").allowedHeaders("*");
			}

		    @Override
		    public void configureAsyncSupport(AsyncSupportConfigurer configurer) {
		        configurer.setDefaultTimeout(300000); // 5 minutos de timeout
		    }

		};

	}


}
