package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.OficinaRegistralBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class OficRegRowMapper implements RowMapper<OficinaRegistralBean> {

    @Override
    public OficinaRegistralBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        OficinaRegistralBean item = new OficinaRegistralBean();
        item.setRegPubId(rs.getString("REG_PUB_ID"));
        item.setOficRegId(rs.getString("OFIC_REG_ID"));
        item.setNombre(rs.getString("NOMBRE"));
        return item;
    }
}
