package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VehiculoBean {
    private String placa;
    private String partida;
    private String refnumPart;
    private String codLibro;
    private String regPubId;
    private String oficRegId;
    private String estado;
}
