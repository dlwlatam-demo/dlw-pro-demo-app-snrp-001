package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class TarifaBean {
    private BigDecimal precOfic;
    private BigDecimal codGrupoLibroArea;
}
