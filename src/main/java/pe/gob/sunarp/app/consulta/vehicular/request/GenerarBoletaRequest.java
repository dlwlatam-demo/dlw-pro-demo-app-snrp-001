package pe.gob.sunarp.app.consulta.vehicular.request;

import lombok.Getter;
import lombok.Setter;
import pe.gob.sunarp.app.consulta.vehicular.bean.VisanetResponseBean;

@Getter
@Setter
public class GenerarBoletaRequest {
    private String usuario;
    private String costo;
    private String ip;
    private String codZona;
    private String codOficina;
    private String placa;
    private VisanetResponseBean visanetResponse;
}
