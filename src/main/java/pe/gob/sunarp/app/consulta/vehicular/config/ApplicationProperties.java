package pe.gob.sunarp.app.consulta.vehicular.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@ConfigurationProperties(prefix = "application.general")
@Component
@Getter
@Setter
public class ApplicationProperties {

    private int querylimit;
    private String imgBackground;
    private String imgInfo;
    private int fontDataSize;
    private String fontDataType;
    private int fontPropSize;
    private String fontPropType;
    private int maxCharline;
    
    private String fontWatMarkType;
    private int fontWatMarkSize;
}
