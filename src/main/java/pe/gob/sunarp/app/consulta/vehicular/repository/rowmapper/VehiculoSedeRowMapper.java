package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoSedeBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiculoSedeRowMapper implements RowMapper<VehiculoSedeBean> {

    @Override
    public VehiculoSedeBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return VehiculoSedeBean.builder()
                .prefijo(rs.getString("prefijo"))
                .regPubId(rs.getString("reg_pub_id"))
                .oficRegId(rs.getString("ofic_reg_id"))
                .nombre(rs.getString("nombre"))
                .fgBaja(rs.getString("fg_baja"))
                .build();
    }
    
    
    
}
