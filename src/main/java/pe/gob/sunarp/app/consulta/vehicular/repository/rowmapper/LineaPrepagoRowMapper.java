package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.LineaPrepagoBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class LineaPrepagoRowMapper implements RowMapper<LineaPrepagoBean> {

    @Override
    public LineaPrepagoBean mapRow(ResultSet rs, int rowNum) throws SQLException {

    	return  new LineaPrepagoBean(rs.getLong("lineaPrepagoId"),
    			rs.getString("estado"),
    			rs.getString("fgDeposito"), 
    			rs.getBigDecimal("saldo"),
    			"",
    			rs.getBigDecimal("cuentaId"),
    			rs.getBigDecimal("peJuriId"),
    			rs.getBigDecimal("juriPersonaId"),
    			rs.getBigDecimal("peNatuId"),
    			rs.getBigDecimal("natuPersonaId")
    			);

     }
}	

