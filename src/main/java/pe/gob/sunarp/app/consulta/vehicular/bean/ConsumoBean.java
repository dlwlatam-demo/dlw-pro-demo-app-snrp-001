package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

@Getter
@Setter
public class ConsumoBean  implements Serializable {
    private static final long serialVersionUID = 1L;
    private long consumoId;
    private BigDecimal monto;
    private String tpoConsumo;

    private long transId;
    private String fecHor;
    private String strBusq;
    private long movimientoId;
}
