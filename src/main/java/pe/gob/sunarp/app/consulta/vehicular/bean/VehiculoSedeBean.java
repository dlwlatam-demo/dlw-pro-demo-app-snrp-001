package pe.gob.sunarp.app.consulta.vehicular.bean;

import java.io.Serializable;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VehiculoSedeBean implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String prefijo;
    private String regPubId;
    private String oficRegId;
    private String nombre;
    private String fgBaja;
}
