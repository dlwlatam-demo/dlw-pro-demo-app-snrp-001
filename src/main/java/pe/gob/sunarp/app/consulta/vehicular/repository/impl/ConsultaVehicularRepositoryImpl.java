package pe.gob.sunarp.app.consulta.vehicular.repository.impl;

import com.google.gson.Gson;
import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.sunarp.app.consulta.vehicular.bean.*;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionProperties;
import pe.gob.sunarp.app.consulta.vehicular.repository.ConsultaVehicularRepository;
import pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper.*;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.*;

@Repository
public class ConsultaVehicularRepositoryImpl extends JdbcDaoSupport implements ConsultaVehicularRepository {

    private static final Logger LOG = LoggerFactory.getLogger(ConsultaVehicularRepositoryImpl.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ExceptionProperties exceptionProperties;

    @Autowired
    public void DataSource(DataSource dataSource) {
        setDataSource(dataSource);
    }

    @Override
    public int countConsulta(String userKeyId, String date) throws ExceptionBean {
        LOG.info("Inicio countConsulta.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(WEBSERVICE)
                .withCatalogName(WEBSERVICES_RESOURCES_BODEGA)
                .withProcedureName(GET_COUNTBYIPAPPANDDATE)
                .declareParameters( new SqlParameter("puser_key_id", OracleTypes.VARCHAR),
                        new SqlParameter("pc_date", OracleTypes.CHAR),
                        new SqlOutParameter("pn_result", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("puser_key_id", userKeyId)
                .addValue("pc_date", date);

        Map<String, Object> out = simpleJdbcCall.execute(inputParameters);
        BigDecimal outParameters = (BigDecimal) out.get("pn_result");

        LOG.info("resultado countConsulta = " + outParameters.intValue());
        return outParameters.intValue();
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<VehiculoDataBean> getVehiculoData(String placa, String codRegion, String codSede) throws ExceptionBean {
        LOG.info("Inicio getVehiculoData: " + placa + " - " + codRegion + " - " +  codSede);
        LOG.info("placa:|" + placa + "|");
        LOG.info("codRegion:|" + codRegion + "|");
        LOG.info("codSede:|" + codSede + "|");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(WEBSERVICE)
                .withCatalogName(WEBSERVICES_RESOURCES_BODEGA)
                .withProcedureName(GET_VEHICULODATOSBODEGA)
                .declareParameters(new SqlParameter("pc_co_regi", OracleTypes.VARCHAR),
                        new SqlParameter("pc_co_sede", OracleTypes.VARCHAR),
                        new SqlParameter("pc_no_plac", OracleTypes.VARCHAR),
                        new SqlOutParameter("pc_cursor", OracleTypes.CURSOR, new VehiculoDataRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pc_co_regi", codRegion)
                .addValue("pc_co_sede", codSede)
                .addValue("pc_no_plac", placa);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<VehiculoDataBean> list = (List<VehiculoDataBean>) outParameters.get("pc_cursor");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getVehiculoData = " + list.size());
        return list;
    }

    @SuppressWarnings("unchecked")
	@Override
    public List<PropietarioBean> getPropietarioVehiculo(String placa, String codRegion, String codSede) throws ExceptionBean {
        LOG.info("Inicio getPropietarioVehiculo.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(WEBSERVICE)
                .withCatalogName(WEBSERVICES_RESOURCES_BODEGA)
                .withProcedureName(GET_VEHICULOPROPIETARIOBODEGA)
                .declareParameters(new SqlParameter("pc_co_regi", OracleTypes.CHAR),
                        new SqlParameter("pc_co_sede", OracleTypes.CHAR),
                        new SqlParameter("pc_no_plac", OracleTypes.CHAR),
                        new SqlOutParameter("pc_cursor", OracleTypes.CURSOR, new PropietarioVehiculoRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pc_co_regi", codRegion)
                .addValue("pc_co_sede", codSede)
                .addValue("pc_no_plac", placa);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<PropietarioBean> list = (List<PropietarioBean>) outParameters.get("pc_cursor");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getPropietarioVehiculo = " + list.size());
        return list;
    }

    @Override
    public int saveConsultaVehicular(String codRegion, String codSede, String placa, String tipoCliente, String ip,
                                     String estado, String idUsu, String userKeyId) throws ExceptionBean {
        LOG.info("Inicio saveConsultaVehicular.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(WEBSERVICE)
                .withCatalogName(WEBSERVICES_RESOURCES_BODEGA)
                .withProcedureName(CTPR_REGCONSULTAAPPS)
                .declareParameters(new SqlParameter("pc_co_regi", OracleTypes.CHAR),
                        new SqlParameter("pc_co_sede", OracleTypes.CHAR),
                        new SqlParameter("pc_no_plac", OracleTypes.CHAR),
                        new SqlParameter("pc_ti_clien", OracleTypes.CHAR),
                        new SqlParameter("pv_ip", OracleTypes.VARCHAR),
                        new SqlParameter("pc_in_esta", OracleTypes.CHAR),
                        new SqlParameter("pc_id_usua_crea", OracleTypes.CHAR),
                        new SqlParameter("pc_user_key_id", OracleTypes.CHAR));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pc_co_regi", codRegion)
                .addValue("pc_co_sede", codSede)
                .addValue("pc_no_plac", placa)
                .addValue("pc_ti_clien", tipoCliente)
                .addValue("pv_ip", ip)
                .addValue("pc_in_esta", estado)
                .addValue("pc_id_usua_crea", idUsu)
                .addValue("pc_user_key_id", userKeyId);

        simpleJdbcCall.execute(inputParameters);

        LOG.info("resultado saveConsultaVehicular.");
        return 1;
    }

    @SuppressWarnings("unchecked")
	@Override
    public VehiculoBean getVehiculo(String zona, String oficina, String placa) throws ExceptionBean {
        LOG.info("Inicio getVehiculo.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SP_GET_VEHICULO)
                .declareParameters(new SqlParameter("pin_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pin_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pin_placa", OracleTypes.VARCHAR),
                        new SqlOutParameter("pc_cursor", OracleTypes.CURSOR, new VehiculoRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_zona", zona)
                .addValue("pin_oficina", oficina)
                .addValue("pin_placa", placa);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<VehiculoBean> list = (List<VehiculoBean>) outParameters.get("pc_cursor");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getVehiculo = " + list.size());
        return list.get(0);
    }

    @SuppressWarnings("unchecked")
	@Override
    public VehiculoPartidaBean getVehiculoXPartida(String placa, String numPart) throws ExceptionBean {
        LOG.info("Inicio getVehiculoXPartida.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SP_GET_NUMPLACA_X_PARTIDA)
                .declareParameters(new SqlParameter("pin_placa", OracleTypes.VARCHAR),
                        new SqlParameter("pin_num_partida", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new VehiculoXPartidaRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_placa", placa)
                .addValue("pin_num_partida", numPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<VehiculoPartidaBean> list = (List<VehiculoPartidaBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getVehiculoXPartida = " + list.size());
        return list.get(0);
    }

    @SuppressWarnings("unchecked")
	@Override
    public TransaccionBean getTransactionById(String transId) throws ExceptionBean {
        LOG.info("Inicio getTransactionById.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_SOLICITUD)
                .withProcedureName(SP_GET_TRANSACTION_BY_ID)
                .declareParameters(new SqlParameter("pin_transId", OracleTypes.NVARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new GetTransactionByIdRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_transId", transId);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<TransaccionBean> list = (List<TransaccionBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getTransactionById = " + list.size());
        return list.get(0);
    }

    @Override
    public long savePago(RegularizaPagoBean regularizaPagoBean) throws ExceptionBean {
        LOG.info("Inicio savePago.");
        LOG.info("getNumOrden() = " + regularizaPagoBean.getVisanetResponse().getNumOrden());
        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_TA_USER_PAYMENT)
                .withProcedureName(INS_TA_USER_PAYMENT)
                .declareParameters( new SqlParameter("in_ID_USER", OracleTypes.NUMBER),
                        new SqlParameter("in_ID_UNICO", OracleTypes.VARCHAR),
                        new SqlParameter("in_PAN", OracleTypes.VARCHAR),
                        new SqlParameter("in_IMP_AUTORIZADO", OracleTypes.NUMBER),
                        new SqlParameter("in_DSC_COD_ACCION", OracleTypes.VARCHAR),
                        new SqlParameter("in_COD_AUTORIZA", OracleTypes.VARCHAR),
                        new SqlParameter("in_CODTIENDA", OracleTypes.VARCHAR),
                        new SqlParameter("in_NUMORDEN", OracleTypes.VARCHAR),
                        new SqlParameter("in_CODACCION", OracleTypes.VARCHAR),
                        new SqlParameter("in_FECHAYHORA_TX", OracleTypes.VARCHAR),
                        new SqlParameter("in_NOM_EMISOR", OracleTypes.VARCHAR),
                        new SqlParameter("in_ORI_TARJETA", OracleTypes.CHAR),
                        new SqlParameter("in_RESPUESTA", OracleTypes.CHAR),
                        new SqlParameter("in_USER_KEY_ID", OracleTypes.VARCHAR),
                        new SqlParameter("in_TRANS_ID", OracleTypes.NUMBER),
                        new SqlParameter("in_ETICKET", OracleTypes.VARCHAR),
                        new SqlParameter("in_CONCEPTO", OracleTypes.VARCHAR),
                        new SqlParameter("in_VISANETRESPONSE", OracleTypes.VARCHAR));

        String numOrden = regularizaPagoBean.getVisanetResponse().getNumOrden().trim();
        if(numOrden.length() > 9) {
        	numOrden = numOrden.substring(9);
        }
        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("in_ID_USER", regularizaPagoBean.getVisanetResponse().getIdUser())
                .addValue("in_ID_UNICO", regularizaPagoBean.getVisanetResponse().getIdUnico())
                .addValue("in_PAN", regularizaPagoBean.getVisanetResponse().getPan())
                .addValue("in_IMP_AUTORIZADO", regularizaPagoBean.getCostoTotal().intValue())
                .addValue("in_DSC_COD_ACCION", regularizaPagoBean.getVisanetResponse().getDscCodAccion())
                .addValue("in_COD_AUTORIZA", regularizaPagoBean.getVisanetResponse().getCodAutoriza())
                .addValue("in_CODTIENDA", regularizaPagoBean.getVisanetResponse().getCodtienda())
                .addValue("in_NUMORDEN", numOrden)
                .addValue("in_CODACCION", regularizaPagoBean.getVisanetResponse().getCodAccion())
                .addValue("in_FECHAYHORA_TX", regularizaPagoBean.getVisanetResponse().getFechaYhoraTx())
                .addValue("in_NOM_EMISOR", regularizaPagoBean.getVisanetResponse().getNomEmisor())
                .addValue("in_ORI_TARJETA", regularizaPagoBean.getVisanetResponse().getOriTarjeta())
                .addValue("in_RESPUESTA", regularizaPagoBean.getVisanetResponse().getRespuesta())
                .addValue("in_USER_KEY_ID", regularizaPagoBean.getUsrKeyId())
                .addValue("in_TRANS_ID", regularizaPagoBean.getTransId())
                .addValue("in_ETICKET", regularizaPagoBean.getVisanetResponse().getEticket())
                .addValue("in_CONCEPTO", regularizaPagoBean.getVisanetResponse().getConcepto())
                .addValue("in_VISANETRESPONSE", new Gson().toJson(regularizaPagoBean.getVisanetResponse(),
                        VisanetResponseBean.class));

        simpleJdbcCall.execute(inputParameters);

        LOG.info("resultado savePago.");
        return 1;
    }

    @Override
    public String getNumPlaca(Long refnumPart) throws ExceptionBean {
        LOG.info("Inicio getNumPlaca.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SF_GET_PLACA)
                .declareParameters( new SqlParameter("pin_refnumPart", OracleTypes.NUMBER),
                        new SqlOutParameter("pon_num_placa", OracleTypes.VARCHAR));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_refnumPart", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        String out = (String) outParameters.get("pon_num_placa");

        if(out.equals("-1")) {
            out = "";
        }

        LOG.info("resultado getNumPlaca = " + outParameters);
        return out;
    }

    @Override
    public String getTipoUso(String codTipoUsos) throws ExceptionBean {
        LOG.info("Inicio getTipoUso.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SF_GET_TIPO_USO)
                .declareParameters( new SqlParameter("pin_cod_tipo", OracleTypes.NUMBER),
                        new SqlOutParameter("pon_desc_tipo_uso", OracleTypes.NVARCHAR));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_cod_tipo", codTipoUsos);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        String out = (String) outParameters.get("pon_desc_tipo_uso");

        if(out.equals("-1")) {
            out = "";
        }

        LOG.info("resultado getTipoUso = " + outParameters);
        return out;
    }

    @Override
    public List<ParticipantesPnBean> getParticipantePn(String zona, String oficina, Long refnumPart, String placa) throws ExceptionBean {
        LOG.info("Inicio getParticipantePn.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_VEHICULO_PARTIDA_PN)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_placa", OracleTypes.VARCHAR),
                        new SqlParameter("pin_refnum_part", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new ParticipantesPnRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_placa", placa)
                .addValue("pin_refnum_part", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<ParticipantesPnBean> list = (List<ParticipantesPnBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getParticipantePn = " + list.size());
        return list;
    }

    @Override
    public List<ParticipantesPjBean> getParticipantePj(String zona, String oficina, Long refnumPart, String placa) throws ExceptionBean {
        LOG.info("Inicio getParticipantePj.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_VEHICULO_PARTIDA_PJ)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_placa", OracleTypes.VARCHAR),
                        new SqlParameter("pic_refnum_part", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new ParticipantesPjRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_placa", placa)
                .addValue("pic_refnum_part", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<ParticipantesPjBean> list = (List<ParticipantesPjBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getParticipantePj = " + list.size());
        return list;
    }

    @Override
    public List<UbigeoBean> getNombreUbigeo(String dpto, String prov, String dist) throws ExceptionBean {
        LOG.info("Inicio getNombreUbigeo.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_PARAMETROS)
                .withProcedureName(SP_LIST_UBIGEO)
                .declareParameters(new SqlParameter("pic_dpto", OracleTypes.VARCHAR),
                        new SqlParameter("pic_prov", OracleTypes.VARCHAR),
                        new SqlParameter("pic_dist", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new UbigeoRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_dpto", dpto)
                .addValue("pic_prov", prov)
                .addValue("pic_dist", dist);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<UbigeoBean> list = (List<UbigeoBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getNombreUbigeo = " + list.size());
        return list;
    }

    @Override
    public List<GravamenVehBean> getGravamenVehicular(String placa, String estado) throws ExceptionBean {
        LOG.info("Inicio getGravamenVehicular.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_LIST_GRAVAMEN)
                .declareParameters(new SqlParameter("pic_numPlaca", OracleTypes.VARCHAR),
                        new SqlParameter("pic_estado", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new GravamenVehRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_numPlaca", placa)
                .addValue("pic_estado", estado);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<GravamenVehBean> list = (List<GravamenVehBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getGravamenVehicular = " + list.size());
        return list;
    }

    @Override
    public List<ParticipantesPnBean> getParticipanteGravamenPn(String zona, String oficina, Long nsGravamen, String placa) throws ExceptionBean {
        LOG.info("Inicio getParticipanteGravamenPn.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_PART_GRAV_PN)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_numPlaca", OracleTypes.VARCHAR),
                        new SqlParameter("pic_ns_gravamen", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new PartGravPnRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_numPlaca", placa)
                .addValue("pic_ns_gravamen", nsGravamen);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<ParticipantesPnBean> list = (List<ParticipantesPnBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getParticipanteGravamenPn = " + list.size());
        return list;
    }

    @Override
    public List<ParticipantesPjBean> getParticipanteGravamenPj(String zona, String oficina, Long nsGravamen, String placa) throws ExceptionBean {
        LOG.info("Inicio getParticipanteGravamenPj.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_PART_GRAV_PJ)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_numPlaca", OracleTypes.VARCHAR),
                        new SqlParameter("pic_ns_gravamen", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new PartGravPjRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_numPlaca", placa)
                .addValue("pic_ns_gravamen", nsGravamen);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<ParticipantesPjBean> list = (List<ParticipantesPjBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getParticipanteGravamenPj = " + list.size());
        return list;
    }

    @Override
    public List<VehiculoHistoricoBean> getVehHistorico(Long refnumPart) throws ExceptionBean {
        LOG.info("Inicio getVehHistorico.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_VEHICULO_HIST)
                .declareParameters(new SqlParameter("pic_refnum_part", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new VehHistoricoRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_refnum_part", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<VehiculoHistoricoBean> list = (List<VehiculoHistoricoBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getVehHistorico = " + list.size());
        return list;
    }

    @Override
    public List<VehiculoPlacaAnteriorBean> getPlacaAnterior(Long refnumPart) throws ExceptionBean {
        LOG.info("Inicio getPlacaAnterior.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_PLACA_ANTERIOR)
                .declareParameters(new SqlParameter("pic_refnum_part", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new VehPlacaAntRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_refnum_part", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<VehiculoPlacaAnteriorBean> list = (List<VehiculoPlacaAnteriorBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getPlacaAnterior = " + list.size());
        return list;
    }

    @Override
    public List<TitulosPendientesBean> getTitulosPendientesVeh(String zona, String oficina, String libro, String partida) throws ExceptionBean {
        LOG.info("Inicio getTitulosPendientesVeh.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_TITULO_PENDIENTE)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_libro", OracleTypes.VARCHAR),
                        new SqlParameter("pic_partida", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new TitulosPendientesRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_libro", libro)
                .addValue("pic_partida", partida);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<TitulosPendientesBean> list = (List<TitulosPendientesBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getTitulosPendientesVeh = " + list.size());
        return list;
    }

    @Override
    public List<ParticipantesPnBean> getPropietarioHistoricoPN(String zona, String oficina, Long refnumPart) throws ExceptionBean {
        LOG.info("Inicio getPropietarioHistoricoPN.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_PROP_VEH_HIST_PN)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_refnum_part", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new PropietarioHistoricoPnRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_refnum_part", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<ParticipantesPnBean> list = (List<ParticipantesPnBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getPropietarioHistoricoPN = " + list.size());
        return list;
    }

    @Override
    public List<ParticipantesPjBean> getPropietarioHistoricoPJ(String zona, String oficina, Long refnumPart) throws ExceptionBean {
        LOG.info("Inicio getPropietarioHistoricoPJ.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_PROP_VEH_HIST_PJ)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_oficina", OracleTypes.VARCHAR),
                        new SqlParameter("pic_refnum_part", OracleTypes.NUMBER),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new PropietarioHistoricoPjRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_oficina", oficina)
                .addValue("pic_refnum_part", refnumPart);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<ParticipantesPjBean> list = (List<ParticipantesPjBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            list = new ArrayList<>();
        }
        LOG.info("resultado getPropietarioHistoricoPJ = " + list.size());
        return list;
    }

    @Override
    public CuentaBean getDatosCuenta(String usuario) throws ExceptionBean {
        LOG.info("Inicio getDatosCuenta.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SP_GET_DATOS_CUENTA)
                .declareParameters(new SqlParameter("pin_usr_id", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new DatosCuentaRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_usr_id", usuario);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<CuentaBean> list = (List<CuentaBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getDatosCuenta = " + list.size());
        return list.get(0);
    }

    @Override
    public OficinaRegistralBean getOficinaRegistral(String zona, String oficina) throws ExceptionBean {
        LOG.info("Inicio getOficinaRegistral.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_PARAMETROS)
                .withProcedureName(SP_GET_OFIC_REG)
                .declareParameters(new SqlParameter("pic_zona", OracleTypes.VARCHAR),
                        new SqlParameter("pic_ofic", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new OficRegRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_zona", zona)
                .addValue("pic_ofic", oficina);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<OficinaRegistralBean> list = (List<OficinaRegistralBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getOficinaRegistral = " + list.size());
        return list.get(0);
    }

    @Override
    public TarifaBean getTarifaOficina(String codGla, Long servicioId) throws ExceptionBean {
        LOG.info("Inicio getTarifaOficina.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_PARAMETROS)
                .withProcedureName(SP_GET_TARIFA_OFIC)
                .declareParameters(new SqlParameter("pic_libro", OracleTypes.VARCHAR),
                        new SqlParameter("pic_servicio", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new TarifaRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pic_libro", codGla)
                .addValue("pic_servicio", servicioId);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<TarifaBean> list = (List<TarifaBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getTarifaOficina = " + list.size());
        return list.get(0);
    }

    @Override
    public long saveTransaccion(TransaccionBean trans) throws ExceptionBean {
        LOG.info("Inicio saveTransaccion.");
        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_SOLICITUD)
                .withProcedureName(SP_INSERT_TRANSACCION)
                .declareParameters(
                        new SqlParameter("pin_servicioId", OracleTypes.NUMBER),
                        new SqlParameter("pin_cuentaId", OracleTypes.NUMBER),
                        new SqlParameter("pin_costo", OracleTypes.NUMBER),
                        new SqlParameter("pin_ip", OracleTypes.VARCHAR),
                        new SqlParameter("pin_tipoUsr", OracleTypes.VARCHAR),
                        new SqlParameter("pin_strBusq", OracleTypes.VARCHAR),
                        new SqlParameter("pin_regPubId", OracleTypes.VARCHAR),
                        new SqlParameter("pin_oficRegId", OracleTypes.VARCHAR),
                        new SqlParameter("pin_codGrupoLibroArea", OracleTypes.NUMBER),
                        new SqlParameter("pin_sessionId", OracleTypes.VARCHAR),
                        new SqlParameter("pin_userKeyId", OracleTypes.VARCHAR),
                        new SqlParameter("pin_blobJson", OracleTypes.BINARY),
                        new SqlOutParameter("pout_transId", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_servicioId", trans.getServicioId())
                .addValue("pin_cuentaId", trans.getCuentaId())
                .addValue("pin_costo", trans.getCosto())
                .addValue("pin_ip", trans.getIp())
                .addValue("pin_tipoUsr", trans.getTipoUsr())
                .addValue("pin_strBusq", trans.getStrBusq())
                .addValue("pin_regPubId", trans.getRegPubId())
                .addValue("pin_oficRegId", trans.getOficRegId())
                .addValue("pin_codGrupoLibroArea", trans.getCodGrupoLibroArea())
                .addValue("pin_sessionId", trans.getSessionId())
                .addValue("pin_userKeyId", trans.getUserKeyId())
                .addValue("pin_blobJson", trans.getBlobJson());

        Map<String, Object> out = simpleJdbcCall.execute(inputParameters);
        BigDecimal outParameters = (BigDecimal) out.get("pout_transId");

        if(outParameters.longValue() <= 0) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado saveTransaccion = " + outParameters.longValue());
        return outParameters.longValue();
    }

    @Override
    public long saveMovimiento(MovimientoBean movimientoBean) throws ExceptionBean {
        LOG.info("Inicio saveMovimiento.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_SOLICITUD)
                .withProcedureName(SP_INSERT_MOVIMIENTO)
                .declareParameters( new SqlParameter("pin_fec_hor", OracleTypes.DATE),
                        new SqlParameter("pin_tpo_mov", OracleTypes.VARCHAR),
                        new SqlParameter("pin_monto_fin", OracleTypes.NUMBER),
                        new SqlParameter("pin_fg_asig", OracleTypes.VARCHAR),
                        new SqlParameter("pin_linea_prepago_id", OracleTypes.VARCHAR),
                        new SqlOutParameter("pin_movimiento_id", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_fec_hor", movimientoBean.getFecHor())
                .addValue("pin_tpo_mov", movimientoBean.getTpoMov())
                .addValue("pin_monto_fin", movimientoBean.getMontoFin())
                .addValue("pin_fg_asig", movimientoBean.getFgAsig())
                .addValue("pin_linea_prepago_id", movimientoBean.getLineaPrepago());

        Map<String, Object> out = simpleJdbcCall.execute(inputParameters);
        BigDecimal outParameters = (BigDecimal) out.get("pin_movimiento_id");

        if(outParameters.longValue() <= 0) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado saveMovimiento = " + outParameters.longValue());
        return outParameters.longValue();
    }

    @Override
    public long saveAbono(AbonoBean abonoBean) throws ExceptionBean {
        LOG.info("Inicio saveAbono.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_SOLICITUD)
                .withProcedureName(SP_INSERT_ABONO)
                .declareParameters( new SqlParameter("pin_tipo_abono", OracleTypes.VARCHAR),
                        new SqlParameter("pin_tipo_vent", OracleTypes.VARCHAR),
                        new SqlParameter("pin_tpo_pag_vent", OracleTypes.VARCHAR),
                        new SqlParameter("pin_usr_caja", OracleTypes.VARCHAR),
                        new SqlParameter("pin_tipo_usr", OracleTypes.VARCHAR),
                        new SqlParameter("pin_monto", OracleTypes.NUMBER),
                        new SqlParameter("pin_movimiento_id", OracleTypes.VARCHAR),
                        new SqlParameter("pin_ofic_reg", OracleTypes.VARCHAR),
                        new SqlParameter("pin_reg_pub", OracleTypes.VARCHAR),
                        new SqlParameter("pin_fg_cierre", OracleTypes.VARCHAR),
                        new SqlParameter("pin_persona_id", OracleTypes.NUMBER),
                        new SqlParameter("pin_ts_crea", OracleTypes.DATE),
                        new SqlParameter("pin_ts_modi", OracleTypes.DATE),
                        new SqlParameter("pin_estado", OracleTypes.VARCHAR),
                        new SqlOutParameter("pout_abono_id", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_tipo_abono", abonoBean.getTipoAbono())
                .addValue("pin_tipo_vent", abonoBean.getTipoVent())
                .addValue("pin_tpo_pag_vent", null)
                .addValue("pin_usr_caja", abonoBean.getUsrCaja())
                .addValue("pin_tipo_usr", abonoBean.getTipoUsr())
                .addValue("pin_monto", abonoBean.getMonto())
                .addValue("pin_movimiento_id", abonoBean.getMovimientoId())
                .addValue("pin_ofic_reg", abonoBean.getOficRegId())
                .addValue("pin_reg_pub", abonoBean.getRegPubId())
                .addValue("pin_fg_cierre", abonoBean.getFgCierre())
                .addValue("pin_persona_id", abonoBean.getPersonaId())
                .addValue("pin_ts_crea", abonoBean.getTsCrea())
                .addValue("pin_ts_modi", abonoBean.getTsModi())
                .addValue("pin_estado", abonoBean.getEstado());

        Map<String, Object> out = simpleJdbcCall.execute(inputParameters);
        BigDecimal outParameters = (BigDecimal) out.get("pout_abono_id");

        if(outParameters.longValue() <= 0) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado saveAbono = " + outParameters.longValue());
        return outParameters.longValue();
    }

    @Override
    public long saveComprobante(ComprobanteBean comprobanteBean) throws ExceptionBean {
        LOG.info("Inicio saveComprobante.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_SOLICITUD)
                .withProcedureName(SP_INSERT_COMPROBANTE)
                .declareParameters( new SqlParameter("pin_abono_id", OracleTypes.NUMBER),
                        new SqlParameter("pin_monto", OracleTypes.NUMBER),
                        new SqlParameter("pin_estado", OracleTypes.VARCHAR),
                        new SqlOutParameter("pout_comprobante_id", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_abono_id", comprobanteBean.getAbonoId())
                .addValue("pin_monto", comprobanteBean.getMonto())
                .addValue("pin_estado", comprobanteBean.getEstado());

        Map<String, Object> out = simpleJdbcCall.execute(inputParameters);
        BigDecimal outParameters = (BigDecimal) out.get("pout_comprobante_id");

        if(outParameters.longValue() <= 0) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado saveComprobante = " + outParameters.longValue());
        return outParameters.longValue();
    }

    @Override
    public long getPersonaIdByLineaPrepagoId(long lineaPrepagoId) throws ExceptionBean {
        LOG.info("Inicio getPersonaIdByLineaPrepagoId.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_SOLICITUD)
                .withFunctionName(SF_GET_PERSONA_ID)
                .declareParameters( new SqlParameter("pin_linea_prepago_id", OracleTypes.VARCHAR));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_linea_prepago_id", lineaPrepagoId);

        BigDecimal outParameters = simpleJdbcCall.executeFunction(java.math.BigDecimal.class, inputParameters);

        if(outParameters.longValue() <= 0) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getPersonaIdByLineaPrepagoId = " + outParameters);
        return outParameters.longValue();
    }

    @Override
    public long saveConsumo(ConsumoBean consumo) throws ExceptionBean {
        LOG.info("Inicio saveConsumo.");
        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SP_INS_CONSUMO)
                .declareParameters(new SqlParameter("pin_monto", OracleTypes.NUMBER),
                        new SqlParameter("pin_movimiento_id", OracleTypes.NUMBER),
                        new SqlParameter("pin_trans_id", OracleTypes.NUMBER),
                        new SqlParameter("pic_tpo_consumo", OracleTypes.VARCHAR),
                        new SqlOutParameter("pon_retorno", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_monto", consumo.getMonto())
                .addValue("pin_movimiento_id", consumo.getMovimientoId())
                .addValue("pin_trans_id", consumo.getTransId())
                .addValue("pic_tpo_consumo", consumo.getTpoConsumo());

        Map<String, Object>  outParameters = simpleJdbcCall.execute(inputParameters);
        LOG.info("resultado guardarConsumo = " + outParameters.get("pon_retorno"));

        BigDecimal previo = (BigDecimal) outParameters.get("pon_retorno");
        if(previo.longValue() == 0) {
            throw exceptionProperties.getErrorConsumo();
        }
        return previo.longValue();

    }

    @Override
    public void actualizarSaldo(LineaPrepagoBean lineaPrepago) throws ExceptionBean {
        LOG.info("Inicio actualizarSaldo.");
        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SP_UPD_LINEA_PREPAGO)
                .declareParameters(new SqlParameter("pin_linea_prepago_id", OracleTypes.NUMBER),
                        new SqlParameter("pin_saldo", OracleTypes.NUMBER),
                        new SqlParameter("pic_fg_deposito", OracleTypes.VARCHAR),
                        new SqlParameter("pic_usuario", OracleTypes.VARCHAR),
                        new SqlOutParameter("pon_retorno", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pin_linea_prepago_id", lineaPrepago.getLineaPrepagoId())
                .addValue("pin_saldo", lineaPrepago.getSaldo())
                .addValue("pic_fg_deposito", lineaPrepago.getFgDeposito())
                .addValue("pic_usuario", lineaPrepago.getUsrUltModif());

        Map<String, Object>  outParameters = simpleJdbcCall.execute(inputParameters);
        LOG.info("resultado actualizarSaldo = " + outParameters.get("pon_retorno"));

        BigDecimal previo = (BigDecimal) outParameters.get("pon_retorno");
        if(previo.longValue() == 0) {
            throw exceptionProperties.getErrorLineaPrepago();
        }
    }

    @Override
    public LineaPrepagoBean obtenerLineaPrepago(long idLineaPrepago) throws ExceptionBean {
        LOG.info("Inicio obtenerLineaPrepago.");
        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);
        LOG.info("obtenerLineaPrepago: " + idLineaPrepago);
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_CONSULTAS)
                .withProcedureName(SP_GET_LINEA_PREPAGO )
                .declareParameters(new SqlParameter("pin_id_linea", OracleTypes.NUMBER),
                        new SqlParameter("pic_user_id", OracleTypes.VARCHAR),
                        new SqlOutParameter("pocur", OracleTypes.CURSOR, new LineaPrepagoRowMapper()));

        SqlParameterSource	 inputParameters = new MapSqlParameterSource()
                .addValue("pin_id_linea", idLineaPrepago)
                .addValue("pic_user_id", "");

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<LineaPrepagoBean> list = (List<LineaPrepagoBean>) outParameters.get("pocur");

        if(list == null || list.isEmpty()) {
            return null;
        }
        return list.get(0);


    }
    
    

    @SuppressWarnings("unchecked")
	@Override
    public VehiculoSedeBean getVehiculoSede(String placa) throws ExceptionBean {
        LOG.info("Inicio getVehiculoSede.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(SP_GET_SEDE)
                .declareParameters(new SqlParameter("pc_no_plac", OracleTypes.VARCHAR),
                        new SqlOutParameter("pc_cursor", OracleTypes.CURSOR, new VehiculoSedeRowMapper()));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("pc_no_plac", placa);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<VehiculoSedeBean> list = (List<VehiculoSedeBean>) outParameters.get("pc_cursor");

        if(list == null || list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }
        LOG.info("resultado getVehiculoSede = " + list.size());
        return list.get(0);
    }
    
}
