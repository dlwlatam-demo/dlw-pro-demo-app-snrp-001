package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.TarifaBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TarifaRowMapper implements RowMapper<TarifaBean> {

    @Override
    public TarifaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return TarifaBean.builder()
                .precOfic(rs.getBigDecimal("PREC_OFIC"))
                .codGrupoLibroArea(rs.getBigDecimal("COD_GRUPO_LIBRO_AREA"))
                .build();
    }
}
