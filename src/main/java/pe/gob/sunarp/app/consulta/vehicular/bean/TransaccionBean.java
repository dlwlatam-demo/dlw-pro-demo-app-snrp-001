package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class TransaccionBean {
    private long transId;
    private String fecHor;
    private int servicioId;
    private long cuentaId;
    private String strBusq;
    private String ip;
    private BigDecimal codGrupoLibroArea;
    private String sessionId;
    private String userKeyId;
    private String tipoUsr;
    private String oficRegId;
    private String regPubId;
    private BigDecimal costo;
    private byte[] blobJson;
}
