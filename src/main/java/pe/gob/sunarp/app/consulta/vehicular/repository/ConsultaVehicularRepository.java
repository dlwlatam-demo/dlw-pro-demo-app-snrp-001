package pe.gob.sunarp.app.consulta.vehicular.repository;

import pe.gob.sunarp.app.consulta.vehicular.bean.*;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;

import java.util.List;

public interface ConsultaVehicularRepository {
    int countConsulta(String userKeyId, String date) throws ExceptionBean;
    List<VehiculoDataBean> getVehiculoData(String placa, String codRegion, String codSede) throws ExceptionBean;
    List<PropietarioBean> getPropietarioVehiculo(String placa, String codRegion, String codSede) throws ExceptionBean;
    int saveConsultaVehicular(String codRegion, String codSede, String placa, String tipoCliente, String ip,
                              String estado, String idUsu, String userKeyId) throws ExceptionBean;
    VehiculoBean getVehiculo(String zona, String oficina, String placa) throws ExceptionBean;
    VehiculoPartidaBean getVehiculoXPartida(String placa, String numPart) throws ExceptionBean;
    TransaccionBean getTransactionById(String transId) throws ExceptionBean;
    long savePago(RegularizaPagoBean regularizaPagoBean) throws ExceptionBean;
    String getNumPlaca(Long refnumPart) throws ExceptionBean;
    String getTipoUso(String codTipoUsos) throws ExceptionBean;
    List<ParticipantesPnBean> getParticipantePn(String zona, String oficina, Long refnumPart, String placa) throws ExceptionBean;
    List<ParticipantesPjBean> getParticipantePj(String zona, String oficina, Long refnumPart, String placa) throws ExceptionBean;
    List<UbigeoBean> getNombreUbigeo(String dpto, String prov, String dist)throws ExceptionBean;
    List<GravamenVehBean> getGravamenVehicular(String placa, String estado) throws ExceptionBean;
    List<ParticipantesPnBean> getParticipanteGravamenPn(String zona, String oficina, Long nsGravamen, String placa) throws ExceptionBean;
    List<ParticipantesPjBean> getParticipanteGravamenPj(String zona, String oficina, Long nsGravamen, String placa) throws ExceptionBean;
    List<VehiculoHistoricoBean> getVehHistorico(Long refnumPart) throws ExceptionBean;
    List<VehiculoPlacaAnteriorBean> getPlacaAnterior(Long refnumPart) throws ExceptionBean;
    List<TitulosPendientesBean> getTitulosPendientesVeh(String zona, String oficina, String libro, String partida) throws ExceptionBean;
    List<ParticipantesPnBean> getPropietarioHistoricoPN(String zona, String oficina, Long refnumPart) throws ExceptionBean;
    List<ParticipantesPjBean> getPropietarioHistoricoPJ(String zona, String oficina, Long refnumPart) throws ExceptionBean;
    CuentaBean getDatosCuenta(String usuario) throws ExceptionBean;
    OficinaRegistralBean getOficinaRegistral(String zona, String oficina) throws ExceptionBean;
    TarifaBean getTarifaOficina(String codGla, Long servicioId) throws ExceptionBean;
    long saveTransaccion(TransaccionBean trans) throws ExceptionBean;
    long saveMovimiento(MovimientoBean movimientoBean) throws ExceptionBean;
    long saveAbono(AbonoBean abonoBean) throws ExceptionBean;
    long saveComprobante(ComprobanteBean comprobanteBean) throws ExceptionBean;
    long getPersonaIdByLineaPrepagoId(long lineaPrepagoId) throws ExceptionBean;
    long saveConsumo(ConsumoBean consumo) throws ExceptionBean;
    void actualizarSaldo(LineaPrepagoBean lineaPrepago) throws ExceptionBean;
    LineaPrepagoBean obtenerLineaPrepago(long idLineaPrepago) throws ExceptionBean;

    VehiculoSedeBean getVehiculoSede(String placa) throws ExceptionBean;
}
