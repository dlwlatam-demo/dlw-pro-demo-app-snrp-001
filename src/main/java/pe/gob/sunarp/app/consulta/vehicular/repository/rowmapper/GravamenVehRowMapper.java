package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.GravamenVehBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class GravamenVehRowMapper implements RowMapper<GravamenVehBean> {

    @Override
    public GravamenVehBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        GravamenVehBean item = new GravamenVehBean();
        item.setNsGravamen(rs.getLong("NS_GRAVAMEN"));
        item.setAnoTitu(rs.getString("ANO_TITU"));
        item.setNumTitu(rs.getString("NUM_TITU"));
        item.setDescTipoAfec(rs.getString("DESCRIPCION"));
        item.setJuzg(rs.getString("JUZG"));
        item.setCausAfec(rs.getString("CAUS_AFEC"));
        item.setJuezAfec(rs.getString("JUEZ_AFEC"));
        item.setSecrAfect(rs.getString("SECR_AFEC"));
        item.setJuezDesc(rs.getString("JUEZ_DESC"));
        item.setSecrDesc(rs.getString("SECR_DESC"));
        item.setFgEstado(rs.getString("FG_ESTADO"));
        item.setNumExpeAfec(rs.getString("NUM_EXPE_AFEC"));
        item.setNumExpeDesc(rs.getString("NUM_EXPE_DESC"));
        item.setTsAfec(rs.getDate("TS_AFEC"));
        item.setTsExpeDesc(rs.getDate("TS_EXPE_DESC"));
        item.setTsProcDesc(rs.getDate("TS_PROC_DESC"));
        item.setTsExpeAfec(rs.getDate("TS_EXPE_AFEC"));
        item.setFgPrenda(rs.getString("FG_PRENDA"));
        item.setNsActo(rs.getBigDecimal("NS_ACTO"));
        item.setAnoTituDesc(rs.getString("ANO_TITU_DESC"));
        item.setNumTituDesc(rs.getString("NUM_TITU_DESC"));
        item.setNsActoDesc(rs.getBigDecimal("NS_ACTO_DESC"));
        item.setAnoTituModi(rs.getString("ANO_TITU_MODI"));
        item.setNumTituModi(rs.getString("NUM_TITU_MODI"));
        item.setNsActoModi(rs.getBigDecimal("NS_ACTO_MODI"));
        return item;
    }
}