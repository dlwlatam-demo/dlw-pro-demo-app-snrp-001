package pe.gob.sunarp.app.consulta.vehicular.service;

import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.BuscarTIVeResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.ProcesoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeResponse;

import java.util.List;

public interface ConsultaTiveService {
    BuscarTIVeResponse buscarTive(TIVeRequest tiveRequest) throws ExceptionBean;
    List<TIVeResponse> buscarHistorial(String jti) throws ExceptionBean;
    ProcesoResponse insertarHistorial(String jti, TIVeRequest tiveRequest) throws ExceptionBean;
}
