package pe.gob.sunarp.app.consulta.vehicular.cache;


import pe.gob.sunarp.app.seguridad.bean.UsuarioJtiBean;
import pe.gob.sunarp.app.consulta.vehicular.bean.BoletaVehicularRedis;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;

public interface RedisService {

    Integer getTokenCache(String jti) throws ExceptionBean;
    
    UsuarioJtiBean getUsuarioJti(String jti) throws ExceptionBean;
    
    BoletaVehicularRedis saveBoletaVehicularRedis(BoletaVehicularRedis data) throws ExceptionBean;
    
    BoletaVehicularRedis getBoletaVehicularRedis(String guid) throws ExceptionBean;
    
}
