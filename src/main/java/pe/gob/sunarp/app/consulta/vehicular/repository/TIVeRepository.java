package pe.gob.sunarp.app.consulta.vehicular.repository;

import pe.gob.sunarp.app.consulta.vehicular.bean.TIVeBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeRequest;

import java.util.List;

public interface TIVeRepository {
	List<TIVeBean> getTIVeUserHistory(Long userId) throws ExceptionBean;
	int countTIVeHistory(Long userId, TIVeRequest tiveRequest) throws ExceptionBean;
	void saveTIVeHistory(Long userId, TIVeRequest tiveRequest) throws ExceptionBean;
}
