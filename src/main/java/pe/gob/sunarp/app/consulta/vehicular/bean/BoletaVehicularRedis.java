package pe.gob.sunarp.app.consulta.vehicular.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;
@Getter
@Setter
public class BoletaVehicularRedis implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private String guid;
	private String status;
	private long transId;

}
