import http from 'k6/http';
import { check } from 'k6';

export let options = {
  duration: '30s',
  vus: 10,
  thresholds: {
    http_req_duration: ['p(95)<500'], // 95 percent of response times must be below 500ms
  },
  ext: {
    loadimpact: {
      // Project: Default project
      projectID: 3686241,
      // Test runs with the same name groups test runs together.
      name: 'Test hello world'
    }
  }  
};

export default function () {
  let response = http.get('http://localhost:8080/appsunarp/api/v1/consulta-tive/historial/busqueda');
  check(response, {
    'status is 200': (r) => r.status === 200,
  });
}
