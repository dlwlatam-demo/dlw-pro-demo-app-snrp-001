package pe.gob.sunarp.app.consulta.vehicular.exception;

import lombok.Getter;
import lombok.Setter;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@ConfigurationProperties(prefix = "appsunarp.error")
@Component
@Getter
@Setter
public class ExceptionProperties {

    private ExceptionBean unauthorized;
    private ExceptionBean notFound;
    private ExceptionBean queryLimit;
    private ExceptionBean errorConsumo;
    private ExceptionBean errorLineaPrepago;
    private ExceptionBean errorWrittingImage;
    private ExceptionBean notFoundVehXPartida;
    private ExceptionBean errorValidation;
    private ExceptionBean errorGettingDoc;
    private ExceptionBean errorMissingRequest;
    private ExceptionBean alreadySavedTIVeHistory;

}
