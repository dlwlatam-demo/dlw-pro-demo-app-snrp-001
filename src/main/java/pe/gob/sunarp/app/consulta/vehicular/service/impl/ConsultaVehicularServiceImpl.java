package pe.gob.sunarp.app.consulta.vehicular.service.impl;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import pe.gob.sunarp.app.consulta.vehicular.bean.*;
import pe.gob.sunarp.app.consulta.vehicular.cache.RedisService;
import pe.gob.sunarp.app.consulta.vehicular.config.ApplicationProperties;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionProperties;
import pe.gob.sunarp.app.consulta.vehicular.mapper.DatosBoletaMapper;
import pe.gob.sunarp.app.consulta.vehicular.repository.ConsultaVehicularRepository;
import pe.gob.sunarp.app.consulta.vehicular.repository.UsuarioAppRepository;
import pe.gob.sunarp.app.consulta.vehicular.request.GenerarBoletaRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.GetDatosVehiculoRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.ValidarBoletaRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.GenerarBoletaResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.GetDatosVehiculoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.ProcesoResponse;
import pe.gob.sunarp.app.consulta.vehicular.service.ConsultaVehicularService;
import pe.gob.sunarp.app.consulta.vehicular.util.Constantes;
import pe.gob.sunarp.app.consulta.vehicular.util.Email;


import pe.gob.sunarp.app.seguridad.bean.UsuarioJtiBean;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.*;

@Slf4j
@Service
public class ConsultaVehicularServiceImpl implements ConsultaVehicularService {

    private static final Logger LOG = LoggerFactory.getLogger(ConsultaVehicularServiceImpl.class);
    @Autowired
    private RedisService redisService;

    @Autowired
    private ExceptionProperties exceptionProperties;

    @Autowired
    private ApplicationProperties applicationProperties;

    @Autowired
    private ConsultaVehicularRepository consultaVehicularRepository;

    @Autowired
    private UsuarioAppRepository usuarioAppRepository;
    
    @Autowired
    private Email email;
    
    @Autowired
    private DatosBoletaMapper datosBoletaMapper;
    

    @Autowired
    private ApplicationContext context;

    private RestTemplate restTemplate;
    

    @Override
    public GetDatosVehiculoResponse getDatosVehiculo(GetDatosVehiculoRequest request, String jti) throws ExceptionBean {
        UsuarioJtiBean usuario = redisService.getUsuarioJti(jti);
        int count = consultaVehicularRepository.countConsulta(usuario.getUserKeyId(), getTodayDate());
        if (count > applicationProperties.getQuerylimit()) {
            throw exceptionProperties.getQueryLimit();
        }
        String placa = this.convertPlateNumber(request.getNuPlac());
        LOG.info("placa = " + placa);
        
        VehiculoSedeBean sede = consultaVehicularRepository.getVehiculoSede(placa);
        
        request.setCodigoRegi(sede.getRegPubId()); 
        request.setCodigoSede(sede.getOficRegId());
        
        List<VehiculoDataBean> vehiculos = consultaVehicularRepository.getVehiculoData(
                placa, request.getCodigoRegi(), request.getCodigoSede());

        String imagenDatos = "";
        for(VehiculoDataBean vehiculo : vehiculos) {
            List<PropietarioBean> propietarios = consultaVehicularRepository.getPropietarioVehiculo(
                    placa, request.getCodigoRegi(), request.getCodigoSede());
            String[] props = this.ordenarPropietarios(propietarios);
            LOG.info("iniciando imagen...");
            try{
                InputStream isBack = ConsultaVehicularServiceImpl.class.getResourceAsStream(applicationProperties.getImgBackground());
                BufferedImage imgBack = ImageIO.read(isBack);
                Graphics2D gBack = imgBack.createGraphics();

                LOG.info("getImgBackground: " + applicationProperties.getImgBackground());
                InputStream is = ConsultaVehicularServiceImpl.class.getResourceAsStream(applicationProperties.getImgInfo());
                BufferedImage img = ImageIO.read(is);
                Graphics2D g = img.createGraphics();
                
                LOG.info("getImgInfo: " + applicationProperties.getImgInfo());

                int x = 200;
                int y = 185;
                int c = 25;

                Font font = new Font(applicationProperties.getFontDataType(), Font.PLAIN, applicationProperties.getFontDataSize());
                g.setFont(font);
                g.setColor(Color.black);
                RenderingHints rh = new RenderingHints(
                        RenderingHints.KEY_TEXT_ANTIALIASING,
                        RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g.setRenderingHints(rh);
                LOG.info("drawString");
                if(vehiculo.getNo_plac() != null && !vehiculo.getNo_plac().isEmpty())
                	g.drawString(vehiculo.getNo_plac(), x, y);
                if(vehiculo.getNo_seri() != null && !vehiculo.getNo_seri().isEmpty())
                	g.drawString(vehiculo.getNo_seri(), x, y + c);
                if(vehiculo.getNo_vin() != null && !vehiculo.getNo_vin().isEmpty())
                	g.drawString(vehiculo.getNo_vin(), x, y + c * 2);
                if(vehiculo.getNo_motr() != null && !vehiculo.getNo_motr().isEmpty())
                	g.drawString(vehiculo.getNo_motr(), x, y + c * 3);
                if(vehiculo.getColor() != null && !vehiculo.getColor().isEmpty())
                	g.drawString(vehiculo.getColor(), x, y + c * 4);
                if(vehiculo.getMarca() != null && !vehiculo.getMarca().isEmpty())
                	g.drawString(vehiculo.getMarca(), x, y + c * 5);
                if(vehiculo.getModelo() != null && !vehiculo.getModelo().isEmpty())
                	g.drawString(vehiculo.getModelo(), x, y + c * 6);
                if(vehiculo.getPlac_vige() != null && !vehiculo.getPlac_vige().isEmpty()) {
                	g.drawString(vehiculo.getPlac_vige(), x, y + c * 7);                
                }
	            else {
	            	g.drawString(placa, x, y + c * 7);                		
	            }

                if(vehiculo.getPlac_ante() != null && !vehiculo.getPlac_ante().isEmpty()) 
                	g.drawString(vehiculo.getPlac_ante(), x, y + c * 8);
                if(vehiculo.getEstado() != null && !vehiculo.getEstado().isEmpty())
                	g.drawString(vehiculo.getEstado(), x, y + c * 9);
                if(vehiculo.getAnotacion() != null && !vehiculo.getAnotacion().isEmpty())
                	g.drawString(vehiculo.getAnotacion(), x, y + c * 10);
                if(vehiculo.getReparticion() != null && !vehiculo.getReparticion().isEmpty())
                	g.drawString(vehiculo.getReparticion(), x, y + c * 11);

                int j = 0;
                for(String propi : props){
                	if(propi != null && !propi.isEmpty()){
                        if(j < 3){
                            g.drawString(propi, 10, y + c * (14 + j));
                            j = j + 1;
                        } else {
                            g.drawString(propi.concat(" ..."), 10, y + c * (14 + j));
                            break;
                        }
                    }
                }

                
                Font font2 = new Font(applicationProperties.getFontPropType(), Font.PLAIN, applicationProperties.getFontPropSize());
                g.setFont(font2);
                g.setColor(Color.black);
                g.drawString(this.getTodayDateTime(), 4, 620);
                
                String theStringWatermark = String.valueOf(usuario.getEmail().hashCode()).replace("-", "") + "-" + 
                		String.format("%08d", usuario.getIdUser());
                LOG.info("theStringWatermark: " + theStringWatermark);
                Font font3 = new Font(applicationProperties.getFontWatMarkType(), Font.BOLD, applicationProperties.getFontWatMarkSize());
                AffineTransform affineTransform = new AffineTransform();
                affineTransform.rotate(Math.toRadians(-45), 0, 0);
                Font rotatedFont = font3.deriveFont(affineTransform);
                g.setFont(rotatedFont);
                g.setColor(new Color(204, 0, 0, 100)); 
                g.drawString(theStringWatermark, 90, 580);

                g.dispose();
                gBack.drawImage(img,0,0,null);
                
                LOG.info("drawString");
                ByteArrayOutputStream os = new ByteArrayOutputStream();
                ImageIO.write(imgBack, "png", os);
                InputStream is2 = new ByteArrayInputStream(os.toByteArray());
                imagenDatos = Base64.getEncoder().encodeToString(this.readAllBytes(is2));
                isBack.close();
                is.close();
                os.close();
                is2.close();
            }catch (Exception e){
                log.error("Error escribiendo imagen. " + e.getMessage());
                e.printStackTrace();
                throw exceptionProperties.getErrorWrittingImage();
            }
        }
        consultaVehicularRepository.saveConsultaVehicular(request.getCodigoRegi(),request.getCodigoSede(), placa,
                "A", request.getIpAddress(), "0", "99999", usuario.getUserKeyId());

        GetDatosVehiculoResponse res = new GetDatosVehiculoResponse();
        res.setDatosVehiculo(imagenDatos);
        return res;
    }

    @Override
    public ProcesoResponse validarBoleta(ValidarBoletaRequest validarBoletaRequest) throws ExceptionBean {

        String placa = this.convertPlateNumber(validarBoletaRequest.getPlaca());
        VehiculoSedeBean sede = consultaVehicularRepository.getVehiculoSede(placa);

        VehiculoBean vehiculo = consultaVehicularRepository.getVehiculo(sede.getRegPubId(), 
                sede.getOficRegId(),  placa);
        if (vehiculo.getPlaca() == null || vehiculo.getPartida() == null){
            throw exceptionProperties.getNotFound();
        }
        VehiculoPartidaBean vehiculoPartida = consultaVehicularRepository.getVehiculoXPartida(vehiculo.getPlaca(),
                vehiculo.getRefnumPart());
        if(vehiculoPartida == null || vehiculoPartida.getNumPlaca() == null) {// !Optional.ofNullable(vehiculoPartida.getNumPlaca()).isPresent()) {
            throw exceptionProperties.getNotFoundVehXPartida();
        }
        

        validarBoletaRequest.setCodOficina(sede.getOficRegId());
        validarBoletaRequest.setCodZona(sede.getRegPubId());
        ObjectMapper mapper = new ObjectMapper();
        String datosBoletaJson = "DatosBoleta";
        try {
        	datosBoletaJson = mapper.writeValueAsString(validarBoletaRequest);
        } catch (JsonGenerationException e) {
            log.error(e.getMessage());
        } catch (JsonMappingException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    	TaAppsunarpLogBean appLog = new TaAppsunarpLogBean();
        String methodName = this.getClass().getSimpleName() + "::"
                + (new Object(){}.getClass().getEnclosingMethod().getName());
        appLog.setIdUsuarioCrea(validarBoletaRequest.getUsuario() ); 
        appLog.setCreatedAt(new Date());
        appLog.setUserKeyId(validarBoletaRequest.getUserKeyId());
        appLog.setWsMethod(methodName + "::ValidarBoletaRequest");
        appLog.setJsonRequest(datosBoletaJson);
        appLog.setIpAddress(validarBoletaRequest.getIpAddress());
        LOG.info("guardarAppLog");
        usuarioAppRepository.guardarAppLog(appLog);
    	
        
        ProcesoResponse response = new ProcesoResponse();
        response.setCodResult("1");
        response.setMsgResult("Exito");
        return response;
    }

    @Override
    public GenerarBoletaResponse generarBoleta(GenerarBoletaRequest generarBoletaRequest, String jti) throws ExceptionBean {
        UsuarioJtiBean usuario = redisService.getUsuarioJti(jti);

		ObjectMapper mapper1 = new ObjectMapper();
		String datosBeanJson = "RegularizaPagoBean";
	
		try {
			datosBeanJson = mapper1.writeValueAsString(generarBoletaRequest);
		} catch (JsonGenerationException e) {
			LOG.error(e.getMessage());
		} catch (JsonMappingException e) {
			LOG.error(e.getMessage());
		} catch (IOException e) {
			LOG.error(e.getMessage());
		}
		LOG.info("generarBoletaRequest = " + datosBeanJson);

        
        generarBoletaRequest.getVisanetResponse().setTransId("0");
        String placa = this.convertPlateNumber(generarBoletaRequest.getPlaca());
        VehiculoSedeBean sede = consultaVehicularRepository.getVehiculoSede(placa);
        
        generarBoletaRequest.setPlaca(placa);
        generarBoletaRequest.setCodZona(sede.getRegPubId());
        generarBoletaRequest.setCodOficina(sede.getOficRegId());
        VehiculoBean vehiculo = consultaVehicularRepository.getVehiculo(generarBoletaRequest.getCodZona(),
                generarBoletaRequest.getCodOficina(), generarBoletaRequest.getPlaca());
        if (vehiculo.getPlaca() == null || vehiculo.getPartida() == null){
            throw exceptionProperties.getNotFound();
        }
        String flag = "";
        LOG.info("estado: " + vehiculo.getEstado());
        if (vehiculo.getEstado().equals("1")) {
            flag = "1";
        } else {
            flag = "2";
        }
        VehiculoPartidaBean vehiculoPartida = consultaVehicularRepository.getVehiculoXPartida(vehiculo.getPlaca(),
                vehiculo.getRefnumPart());

        PartidaDirectaDetBean partidaDetveh = this.obtenerDetalleVehiculo(vehiculoPartida,
                generarBoletaRequest.getCodZona(),
                generarBoletaRequest.getCodOficina(),
                String.valueOf(vehiculo.getRefnumPart()),
                vehiculo.getPlaca(),
                vehiculo.getPartida(),
                vehiculo.getCodLibro(),
                flag);

        DatosBoletaRPV datosBoletaRPV = datosBoletaMapper.toDatosBoletaRPV(generarBoletaRequest);
        LOG.info("usuario = " + datosBoletaRPV.getUsuario());
        datosBoletaRPV.setUsuario(generarBoletaRequest.getUsuario());
        datosBoletaRPV.setCodZona(generarBoletaRequest.getCodZona());
        datosBoletaRPV.setCodOficina(generarBoletaRequest.getCodOficina());
        datosBoletaRPV.setPlaca(generarBoletaRequest.getPlaca());
        datosBoletaRPV.setCosto(new BigDecimal(generarBoletaRequest.getCosto()));
        datosBoletaRPV.setIp(generarBoletaRequest.getIp());
        
        datosBoletaRPV.setDatosBoletaRPV(partidaDetveh);
        LOG.info("setDatosBoletaRPV");
        ObjectMapper mapper = new ObjectMapper();
        String datosRPVJson = "DatosBoletaRPV";
        try {
            datosRPVJson = mapper.writeValueAsString(datosBoletaRPV);
        } catch (JsonGenerationException e) {
            log.error(e.getMessage());
        } catch (JsonMappingException e) {
            log.error(e.getMessage());
        } catch (IOException e) {
            log.error(e.getMessage());
        }
        TaAppsunarpLogBean appLog = new TaAppsunarpLogBean();
        // LOG.info("getUserByKeyId");
     // UsuarioAppBean user = usuarioAppRepository.getUserByKeyId(usuario.getUserKeyId());
        String methodName = this.getClass().getSimpleName() + "::"
                + (new Object(){}.getClass().getEnclosingMethod().getName());
        appLog.setIdUsuarioCrea(generarBoletaRequest.getUsuario() );// user.getIdUsuaCrea());
        appLog.setCreatedAt(new Date());
        appLog.setUserKeyId(usuario.getUserKeyId());
        appLog.setWsMethod(methodName + "::DatosBoletaRPV");
        appLog.setJsonRequest(datosRPVJson);
        appLog.setIpAddress(generarBoletaRequest.getIp());
        LOG.info("guardarAppLog");
        usuarioAppRepository.guardarAppLog(appLog);

        LOG.info("guardarTransaccionBoletaInformativa");
        int transId = this.guardarTransaccionBoletaInformativa(datosBoletaRPV, usuario);

        LOG.info("getTransactionById");
        TransaccionBean transaccionBean = consultaVehicularRepository.getTransactionById(String.valueOf(transId));
        generarBoletaRequest.getVisanetResponse().setTransId(String.valueOf(transId));
        RegularizaPagoBean regularizaPagoBean = new RegularizaPagoBean();
        regularizaPagoBean.setUsrKeyId(usuario.getUserKeyId());
        regularizaPagoBean.setTransId(transId);
        regularizaPagoBean.setVisanetResponse(generarBoletaRequest.getVisanetResponse());
        regularizaPagoBean.setCostoTotal(new BigDecimal(generarBoletaRequest.getCosto()));

        ReciboPublBean resp = new ReciboPublBean();
        resp.setSolicitudId(0);
        resp.setDescripcion(transaccionBean.getStrBusq());
        resp.setTsCrea(transaccionBean.getFecHor());
        resp.setTotal(datosBoletaRPV.getCosto());
        resp.setEmail(usuario.getEmail());
        String nombre = (usuario.getPriApe() != null  ? usuario.getPriApe() + " " : "") +
                (usuario.getSegApe() != null ? usuario.getSegApe() + ", " : "") +
                (usuario.getNombres() != null ? usuario.getNombres() : "");
        resp.setSolicitante(nombre);
        resp.setTpoPago("Tarjeta");

        LOG.info("savePago");
        consultaVehicularRepository.savePago(regularizaPagoBean);
        this.enviarCorreo(regularizaPagoBean, resp);

        GenerarBoletaResponse response = new GenerarBoletaResponse();
        response.setTransId(transId);
        return response;
    }

    private PartidaDirectaDetBean obtenerDetalleVehiculo(VehiculoPartidaBean vehiculoPartida, String codZona,
                                                         String codOficina, String refnumPart, String placa,
                                                         String partida, String codLibro, String flag) throws ExceptionBean {
        PartidaDirectaDetBean p = new PartidaDirectaDetBean();

        p.setNumPlaca(vehiculoPartida.getNumPlaca());
        p.setRefNumPar(String.valueOf(vehiculoPartida.getRefNumPart()));
        p.setNumPartida(vehiculoPartida.getNumPartida());
        p.setOficRegId(vehiculoPartida.getOficRegId());
        p.setAreaRegId(vehiculoPartida.getAreaRegId());
        p.setRegPubId(vehiculoPartida.getRegPubId());
        p.setNomOficina("OFICINA" + " " + vehiculoPartida.getNomOficina());
        String color = "";
        String fechaIns = "";
        if (vehiculoPartida.getColor01() != null) {
            color = color + " " + vehiculoPartida.getColor01();
        }
        if (vehiculoPartida.getColor02() != null) {
            color = color + " " + vehiculoPartida.getColor02();
        }
        if (vehiculoPartida.getColor03() != null) {
            color = color + " " + vehiculoPartida.getColor03();
        }
        p.setColor(color);
        if (vehiculoPartida.getTsInscrip() != null) {
            fechaIns = this.formatDate(vehiculoPartida.getTsInscrip());
        }
        p.setFecIns(fechaIns);
        p.setNoVin(vehiculoPartida.getNoVin() != null ? vehiculoPartida.getNoVin() : "##########");
        p.setCoCateg(vehiculoPartida.getCoCateg() != null ? vehiculoPartida.getCoCateg() : "##########");
        p.setAnMode(vehiculoPartida.getAnMode() != null ? vehiculoPartida.getAnMode() : "##########");
        p.setNoVers(vehiculoPartida.getNoVers() != null ? vehiculoPartida.getNoVers() : "##########");
        p.setPoMotr(vehiculoPartida.getPoMotr() != null ? vehiculoPartida.getPoMotr() : "##########");
        p.setNoClda(vehiculoPartida.getNoClda() != null ? String.valueOf(vehiculoPartida.getNoClda())
                : "##########");
        p.setNoForm(vehiculoPartida.getNoForm() != null ? vehiculoPartida.getNoForm() : "##########");
        p.setColor1(vehiculoPartida.getColor01() != null ? vehiculoPartida.getColor01() : "##########");
        p.setColor2(vehiculoPartida.getColor02() != null ? vehiculoPartida.getColor02() : "##########");
        p.setColor3(vehiculoPartida.getColor03() != null ? vehiculoPartida.getColor03() : "##########");

        if (validaNulo(vehiculoPartida.getDescMarca()))
            p.setMarca(vehiculoPartida.getDescMarca());
        if (validaNulo(vehiculoPartida.getDescTipoVeh()))
            p.setDescTipoVeh(vehiculoPartida.getDescTipoVeh());
        if (validaNulo(vehiculoPartida.getDescTipoComb()))
            p.setDescTipoComb(vehiculoPartida.getDescTipoComb());
        if (validaNulo(vehiculoPartida.getNumSerie()))
            p.setNumSerie(vehiculoPartida.getNumSerie());
        if (validaNulo(vehiculoPartida.getDescCondVeh())) {
            p.setDescCondVeh(vehiculoPartida.getDescCondVeh());
        } else {
            p.setDescCondVeh("---");
        }
        if (validaNulo(vehiculoPartida.getDescCetico())) {
            p.setDescCetico(vehiculoPartida.getDescCetico());
            p.setOptionCetico("1");
        } else {
            p.setOptionCetico("0");
        }
        if (validaNulo(vehiculoPartida.getDescModelo()))
            p.setModelo(vehiculoPartida.getDescModelo());
        if (validaNulo(vehiculoPartida.getDescTipoCarr()))
            p.setDescTipoCarr(vehiculoPartida.getDescTipoCarr());
        if (validaNulo(vehiculoPartida.getNumMotor()))
            p.setNumMotor(vehiculoPartida.getNumMotor());
        if (validaNulo(vehiculoPartida.getAnoFab()))
            p.setAnoFab(vehiculoPartida.getAnoFab());
        if (validaNulo(vehiculoPartida.getAncho()))
            p.setAncho(vehiculoPartida.getAncho());
        if (validaNulo(vehiculoPartida.getNumAsientos()))
            p.setNumAsientos(vehiculoPartida.getNumAsientos());
        if (validaNulo(vehiculoPartida.getNumRuedas()))
            p.setNumRuedas(vehiculoPartida.getNumRuedas());
        if (validaNulo(vehiculoPartida.getAltura()))
            p.setAltura(vehiculoPartida.getAltura());
        if (validaNulo(vehiculoPartida.getNumPuertas()))
            p.setNumPuertas(vehiculoPartida.getNumPuertas());
        if (validaNulo(vehiculoPartida.getNumEjes()))
            p.setNumEjes(vehiculoPartida.getNumEjes());
        if (validaNulo(vehiculoPartida.getPesoSeco()))
            p.setPesoSeco(vehiculoPartida.getPesoSeco());
        if (validaNulo(vehiculoPartida.getNumPasajeros()))
            p.setNumPasajeros(vehiculoPartida.getNumPasajeros());
        if (validaNulo(vehiculoPartida.getLongitud()))
            p.setLongitud(vehiculoPartida.getLongitud());
        if (validaNulo(vehiculoPartida.getPesoBruto()))
            p.setPesoBruto(vehiculoPartida.getPesoBruto());
        if (validaNulo(p.getPesoBruto()) && validaNulo(p.getPesoSeco())) {
            p.setPesoUtil((new BigDecimal(vehiculoPartida.getPesoBruto()))
                    .subtract(new BigDecimal(vehiculoPartida.getPesoSeco()))
                    .toPlainString());
        }
        if (validaNulo(String.valueOf(vehiculoPartida.getNumCilindros())))
            p.setNumCilindros(String.valueOf(vehiculoPartida.getNumCilindros()));
        String estado = vehiculoPartida.getFgBaja();
        if (validaNulo(estado)) {
            if (estado.equals("0"))
                p.setEstado("EN CIRCULACION");
            else if (estado.equals("1"))
                p.setEstado("DE BAJA");
        } else {
            p.setEstado("NO ESPECIFICADO");
        }

        if (flag.equals("1")) {
            p.setListProp(null);
            p.setListGrav(null);
            p.setListParAnt(null);
            p.setListPlacasAnteri(null);
            p.setListTituPend(null);
            p.setOptionBaja("1");
            /**** Ver en caso que flag baja sea 1: */
            String numPlaca = consultaVehicularRepository.getNumPlaca(Long.valueOf(refnumPart));
            if (numPlaca.equals("")) {
                p.setNuevaPlaca("No disponible");
            } else {
                p.setNuevaPlaca(numPlaca);
            }
        } else {
            p.setOptionBaja("2");
            /************************* Propietarios ************************************/
            List<ParticipantesBean> listaprop = this.listarPropietariosVeh(codZona, codOficina, refnumPart, placa);

            if (listaprop.size() == 0) {
                p.setFechaPropi("NO ESPECIFICADO");
            } else {
                p.setFechaPropi(listaprop.get(0).getFechaProp());
            }
            p.setListProp(listaprop);
            /*************** Gravamenes **************************/
            // gravamenes vigentes
            List<GravamenVehBean> listGravamen = this.listarGravamenesVeh(codZona, codOficina, placa);
            p.setListGrav(listGravamen);
            /*************** gravamenes levantados **************************/
            List<GravamenVehBean> listGravamenLev = this.listarGravamenesVehLevantados(codZona, codOficina, placa);
            p.setListGravLev(listGravamenLev);
            /************** Motores y numero de serie anteriores **********************/
            List<VehiculoHistoricoBean> listhistorico = this.listarMotorSerieHisVeh(refnumPart);
            p.setListParAnt(listhistorico);
            /*************** Plascas anteriores ****************************************/
            List<VehiculoPlacaAnteriorBean> listplacasAnteriores = this.listarPlacasAnterioesVeh(refnumPart);
            p.setListPlacasAnteri(listplacasAnteriores);
            /**************** Titulos pendientes ************************************************/
            List<TitulosPendientesBean> listTitulosPendientes = this.listarTitulosPendientesVeh(codZona, codOficina,
                    codLibro, partida);
            p.setListTituPend(listTitulosPendientes);
            /***************** Propietarios historicos veh **************************************/
            List<ParticipantesBean> listapropHis = this.listarPropietariosHistVeh(codZona, codOficina, refnumPart);
            p.setListPropHist(listapropHis);
        }
        /*buscamos la descripción del tipo de uso */
        if (Optional.ofNullable(vehiculoPartida.getCoTipoUsos()).isPresent()
                && !vehiculoPartida.getCoTipoUsos().trim().equals("")) {
            String descTipoUso = consultaVehicularRepository.getTipoUso(vehiculoPartida.getCoTipoUsos());
            p.setDescTipoUso(descTipoUso);
        } else {
            p.setDescTipoUso("##########");
        }
        return p;
    }

    private List<ParticipantesBean> listarPropietariosHistVeh(String codZona, String codOficina, String refnumPart) throws ExceptionBean {
        List<ParticipantesPnBean> listPropHistPN = consultaVehicularRepository.getPropietarioHistoricoPN(codZona,
                codOficina, Long.parseLong(refnumPart));
        List<ParticipantesPjBean> listPropHistPJ = consultaVehicularRepository.getPropietarioHistoricoPJ(codZona,
                codOficina, Long.parseLong(refnumPart));
        List<ParticipantesBean> listPropHist = new ArrayList<>();

        String aux = null;
        StringBuffer sb = new StringBuffer();
        // propietario Persona Natural
        for (ParticipantesPnBean pn : listPropHistPN) {
            ParticipantesBean objpropHis = new ParticipantesBean();
            sb.delete(0, sb.length());
            aux = pn.getApePaterno();
            if (aux != null)
                sb.append(aux);
            aux = pn.getApeMaterno();
            if (aux != null)
                sb.append(" ").append(aux);
            aux = pn.getNombres();
            if (aux != null)
                sb.append(", ").append(aux);
            objpropHis.setNombres(sb.toString());

            sb.delete(0, sb.length());
            aux = pn.getTipoDocumento();
            if (aux != null)
                sb.append(aux);
            aux = pn.getNumDocumento();
            if (aux != null)
                sb.append(" ").append(aux);
            objpropHis.setDocumentos(sb.toString());
            aux = pn.getDireccion();
            if (aux != null)
                objpropHis.setDirecc(aux);

            listPropHist.add(objpropHis);
        }

        // Propietario Persona Juridica
        for (ParticipantesPjBean pj : listPropHistPJ) {
            ParticipantesBean objpropHis = new ParticipantesBean();
            aux = pj.getRazonSocial();
            if (aux != null)
                objpropHis.setNombres(aux);

            sb.delete(0, sb.length());
            aux = pj.getTipoDocumento();
            if (aux != null)
                sb.append(aux);
            aux = pj.getNumDocumento();
            if (aux != null)
                sb.append(" ").append(aux);
            objpropHis.setDocumentos(sb.toString());

            aux = pj.getDireccion();
            if (aux != null)
                objpropHis.setDirecc(aux);

            listPropHist.add(objpropHis);
        }
        return listPropHist;
    }

    private List<TitulosPendientesBean> listarTitulosPendientesVeh(String codZona, String codOficina,
                                                                   String codLibro, String partida) throws ExceptionBean {
        List<TitulosPendientesBean> listTituPend = consultaVehicularRepository
                .getTitulosPendientesVeh(codZona, codOficina, codLibro,partida);
        List<TitulosPendientesBean> listTitulosPendientes = new ArrayList<>();

        TitulosPendientesBean objtitupen = null;
        String aux = "";
        for (TitulosPendientesBean t : listTituPend) {
            objtitupen = new TitulosPendientesBean();
            aux = (t.getNumTitulo() == null) ? "---" : t.getNumTitulo();
            objtitupen.setNumTitulo(aux);
            aux = (t.getAno() == null) ? "---" : t.getAno();
            objtitupen.setAno(aux);
            aux = (t.getAreaRegistralId() == null) ? "---" : t
                    .getAreaRegistralId();
            objtitupen.setAreaRegistralId(aux);
            aux = (t.getOficRegId() == null) ? "---" : t.getOficRegId();
            objtitupen.setOficRegId(aux);
            aux = (t.getRegPubId() == null) ? "---" : t.getRegPubId();
            objtitupen.setRegPubId(aux);
            listTitulosPendientes.add(objtitupen);
        }
        return listTitulosPendientes;
    }

    private List<VehiculoPlacaAnteriorBean> listarPlacasAnterioesVeh(String refnumPart) throws ExceptionBean {
        List<VehiculoPlacaAnteriorBean> listplacaAnt = consultaVehicularRepository
                .getPlacaAnterior(Long.parseLong(refnumPart));
        List<VehiculoPlacaAnteriorBean> listplacasAnteriores = new ArrayList<>();
        VehiculoPlacaAnteriorBean objplacAnt = null;

        for (VehiculoPlacaAnteriorBean pa : listplacaAnt) {
            objplacAnt = new VehiculoPlacaAnteriorBean();
            if (validaNulo(pa.getNumPlaca())) {
                objplacAnt.setNumPlaca(pa.getNumPlaca());
            } else {
                objplacAnt.setNumPlaca("---");
            }
            listplacasAnteriores.add(objplacAnt);
        }
        return listplacasAnteriores;
    }

    private List<VehiculoHistoricoBean> listarMotorSerieHisVeh(String refnumPart) throws ExceptionBean {
        List<VehiculoHistoricoBean> listHist = consultaVehicularRepository.getVehHistorico(Long.parseLong(refnumPart));
        List<VehiculoHistoricoBean> listHistorico = new ArrayList<>();
        VehiculoHistoricoBean objvh = null;
        String aux = "";
        for (VehiculoHistoricoBean h : listHist) {
            objvh = new VehiculoHistoricoBean();
            aux = (h.getNumPlaca() == null) ? "---" : h.getNumPlaca();
            objvh.setNumPlaca(aux);
            aux = (String.valueOf(h.getNsPlaca()) == null) ? "---" : String.valueOf(h.getNsPlaca());
            objvh.setNsPlac(aux);
            aux = (h.getNumSerie() == null) ? "---" : h.getNumSerie();
            objvh.setNumSerie(aux);
            aux = (h.getNumMotor() == null) ? "---" : h.getNumMotor();
            objvh.setNumMotor(aux);
            listHistorico.add(objvh);
        }
        return listHistorico;
    }

    private List<GravamenVehBean> listarGravamenesVehLevantados(String codZona, String codOficina, String placa) throws ExceptionBean {
        List<GravamenVehBean> listGrav = consultaVehicularRepository.getGravamenVehicular(placa, FLG_2);
        List<GravamenVehBean> listGravamen = new ArrayList<>();
        GravamenVehBean grav = null;
        String sAux = "";
        int count = 1;
        for (GravamenVehBean g : listGrav) {
            grav = new GravamenVehBean();
            grav.setNsGravamen(g.getNsGravamen());
            grav.setCantGravamen("GRAVAMEN  " + count);
            if (validaNulo(g.getFgEstado())) {
                grav.setFgEstado("LEVANTADO");
            } else {
                grav.setFgEstado("---");
            }
            sAux = g.getDescTipoAfec();
            if (validaNulo(sAux)) {
                grav.setDescTipoAfec(sAux);
            } else {
                grav.setDescTipoAfec("");
            }

            if (g.getTsAfec() != null) {
                grav.setFechAfec(this.formatDateTimesTamp(g.getTsAfec()));
            } else {
                grav.setFechAfec("");
            }

            sAux = g.getNumExpeAfec();
            if (validaNulo(sAux)) {
                grav.setNumExpeAfec(sAux);
            } else {
                grav.setNumExpeAfec("");
            }

            sAux = g.getAnoTitu() + " " + g.getNumTitu();
            if (validaNulo(sAux)) {
                grav.setNumTitu(sAux);
            } else {
                grav.setNumTitu("");
            }

            sAux = g.getJuzg();
            if (validaNulo(sAux)) {
                grav.setJuzg(sAux);
            } else {
                grav.setJuzg("");
            }

            sAux = g.getCausAfec();
            if (validaNulo(sAux)) {
                grav.setCausAfec(sAux);
            } else {
                grav.setCausAfec("");
            }

            sAux = g.getJuezAfec();
            if (validaNulo(sAux)) {
                grav.setJuezAfec(sAux);
            } else {
                grav.setJuezAfec("");
            }

            sAux = g.getSecrAfect();
            if (validaNulo(sAux)) {
                grav.setSecrAfect(sAux);
            } else {
                grav.setSecrAfect("");
            }

            sAux = g.getAnoTituModi() + " " + g.getNumTituModi();
            if (validaNulo(sAux)) {
                grav.setNumTituModi(sAux);
            } else {
                grav.setNumTituModi("");
            }

            sAux = g.getJuezDesc();
            if (validaNulo(sAux)) {
                grav.setJuezDesc(sAux);
            } else {
                grav.setJuezDesc("");
            }

            sAux = g.getSecrDesc();
            if (validaNulo(sAux)) {
                grav.setSecrDesc(sAux);
            } else {
                grav.setSecrDesc("");
            }

            if (g.getTsExpeDesc() != null) {
                grav.setFechExpeDesc(this.formatDateTimesTamp(g.getTsExpeDesc()));
            } else {
                grav.setFechExpeDesc("");
            }

            sAux = g.getNumExpeDesc();
            if (validaNulo(sAux)) {
                grav.setNumExpeDesc(sAux);
            } else {
                grav.setNumExpeDesc("");
            }
            /** Participantes Gravamenes **/
            List<ParticipantesBean> listParticiGrav = this.listarParticiGravVeh(codZona, codOficina,
                    g.getNsGravamen(), placa);
            grav.setListPartGravLevantados(listParticiGrav);
            /** Fin participantes Gravamenes **/
            count++;
            listGravamen.add(grav);
        }
        return listGravamen;
    }

    private List<GravamenVehBean> listarGravamenesVeh(String codZona, String codOficina, String placa) throws ExceptionBean {

        List<GravamenVehBean> listGrav = consultaVehicularRepository.getGravamenVehicular(placa, FLG_1);
        List<GravamenVehBean> listGravamen = new ArrayList<>();

        GravamenVehBean grav = null;
        String sAux = "";
        int count = 1;
        for (GravamenVehBean g : listGrav) {
            grav = new GravamenVehBean();
            grav.setCantGravamen("GRAVAMEN  " + count);
            if (validaNulo(g.getFgEstado())) {
                grav.setFgEstado("VIGENTE");
            } else {
                grav.setFgEstado("---");
            }
            sAux = g.getDescTipoAfec();
            if (validaNulo(sAux)) {
                grav.setDescTipoAfec(sAux.trim());
            } else {
                grav.setDescTipoAfec("");
            }

            if (g.getTsAfec() != null) {
                grav.setFechAfec(this.formatDateTimesTamp(g.getTsAfec()));
            } else {
                grav.setFechAfec("");
            }

            sAux = g.getNumExpeAfec();
            if (validaNulo(sAux)) {
                grav.setNumExpeAfec(sAux.trim());
            } else {
                grav.setNumExpeAfec("");
            }

            sAux = g.getAnoTitu() + " " + g.getNumTitu();
            if (validaNulo(sAux)) {
                grav.setNumTitu(sAux);
            } else {
                grav.setNumTitu("");
            }

            sAux = g.getJuzg();
            if (validaNulo(sAux)) {
                grav.setJuzg(sAux.trim());
            } else {
                grav.setJuzg("");
            }

            sAux = g.getCausAfec();
            if (validaNulo(sAux)) {
                grav.setCausAfec(sAux.trim());
            } else {
                grav.setCausAfec("");
            }

            sAux = g.getJuezAfec();
            if (validaNulo(sAux)) {
                grav.setJuezAfec(sAux.trim());
            } else {
                grav.setJuezAfec("");
            }

            sAux = g.getSecrAfect();
            if (validaNulo(sAux)) {
                grav.setSecrAfect(sAux.trim());
            } else {
                grav.setSecrAfect("");
            }

            sAux = g.getAnoTituModi() + " " + g.getNumTituModi();
            if (validaNulo(sAux)) {
                grav.setNumTituModi(sAux);
            } else {
                grav.setNumTituModi("");
            }

            sAux = g.getJuezDesc();
            if (validaNulo(sAux)) {
                grav.setJuezDesc(sAux.trim());
            } else {
                grav.setJuezDesc("");
            }

            sAux = g.getSecrDesc();
            if (validaNulo(sAux)) {
                grav.setSecrDesc(sAux.trim());
            } else {
                grav.setSecrDesc("");
            }

            if (g.getTsExpeDesc() != null) {
                grav.setFechExpeDesc(this.formatDateTimesTamp(g.getTsExpeDesc()));
            } else {
                grav.setFechExpeDesc("");
            }

            sAux = g.getNumExpeDesc();
            if (validaNulo(sAux)) {
                grav.setNumExpeDesc(sAux.trim());
            } else {
                grav.setNumExpeDesc("");
            }
            /** Participantes Gravamenes **/
            List<ParticipantesBean> listParticiGrav = this.listarParticiGravVeh(codZona, codOficina,
                    g.getNsGravamen(), placa);
            grav.setListPartGravVigentes(listParticiGrav);
            /** Fin participantes Gravamenes **/
            count++;
            listGravamen.add(grav);
        }
        return listGravamen;
    }

    private List<ParticipantesBean> listarParticiGravVeh(String codZona, String codOficina, long nsGravamen, String placa) throws ExceptionBean {
        List<ParticipantesBean> listPartGrav = new ArrayList<>();

        List<ParticipantesPnBean> listParGravPN = consultaVehicularRepository
                .getParticipanteGravamenPn(codZona, codOficina, nsGravamen, placa);
        List<ParticipantesPjBean> listParGravPJ = consultaVehicularRepository
                .getParticipanteGravamenPj(codZona, codOficina, nsGravamen, placa);
        // Participantes Gravamen Persona Natural
        int n = 0;
        String aux = "";
        for (ParticipantesPnBean pn : listParGravPN) {
            aux = "";
            n++;
            StringBuilder concat = new StringBuilder();
            ParticipantesBean objpg = new ParticipantesBean();
            concat.append(n).append(". ");
            aux = pn.getApePaterno();
            concat.append(aux == null ? "" : (new StringBuffer(aux).append(" ")
                    .toString()));
            aux = pn.getApeMaterno();
            concat.append(aux == null ? "" : (new StringBuffer(aux).append(" ")
                    .toString()));
            aux = pn.getNombres();
            concat.append(aux == null ? "" : (new StringBuffer(aux).append(" ")
                    .toString()));
            String nomPartici = concat.toString();
            if (nomPartici.length() == 2) {
                nomPartici = "---";
            }
            objpg.setNomPart(nomPartici);
            aux = pn.getDesPart();
            if (validaNulo(aux)) {
                objpg.setDescripcion(aux);
            } else {
                objpg.setDescripcion("---");
            }
            listPartGrav.add(objpg);
        }

        // Participantes de Gravamen Persona Juridica
        for (ParticipantesPjBean pj : listParGravPJ) {
            aux = "";
            n++;
            StringBuilder concat = new StringBuilder();
            ParticipantesBean objpg = new ParticipantesBean();
            concat.append(String.valueOf(n)).append(". ");
            aux = pj.getRazonSocial();
            if (validaNulo(aux)) {
                concat.append(aux);
            }
            String nomPartici = concat.toString();
            if (nomPartici.length() == 2) {
                nomPartici = "---";
            }
            objpg.setNomPart(nomPartici);
            aux = pj.getDesPart();
            if (validaNulo(aux)) {
                objpg.setDescripcion(aux);
            } else {
                objpg.setDescripcion("---");
            }
            listPartGrav.add(objpg);
        }
        return listPartGrav;
    }

    private List<ParticipantesBean> listarPropietariosVeh(String codZona, String codOficina,
                                                          String refnumPart, String placa) throws ExceptionBean {
        List<ParticipantesPnBean> listPn = consultaVehicularRepository.getParticipantePn(codZona, codOficina,
                Long.parseLong(refnumPart), placa);
        List<ParticipantesPjBean> listPj = consultaVehicularRepository.getParticipantePj(codZona, codOficina,
                Long.parseLong(refnumPart), placa);
        List<ParticipantesBean> listaprop = new ArrayList<>();

        int i = 0;
        int ccont = 0;
        String aux = "";
        String fechapro = "";
        // propietario Persona Natural
        for (ParticipantesPnBean pn : listPn) {
            i++;
            ParticipantesBean objprop = new ParticipantesBean();
            StringBuilder concat = new StringBuilder();
            concat.append(i).append(". ");
            aux = pn.getApePaterno();
            concat.append(aux == null ? "" : (new StringBuffer(aux).append(" ").toString()));
            aux = pn.getApeMaterno();
            concat.append(aux == null ? "" : (new StringBuffer(aux).append(" ").toString()));
            aux = pn.getNombres();
            concat.append(aux == null ? "" : (new StringBuffer(aux).append(" ").toString()));
            objprop.setPropietario(concat.toString());
            aux = pn.getUbigeo();
            if (validaNulo(aux))
                objprop.setUbigeo(aux);
            aux = pn.getDireccion();
            if (validaNulo(aux))
                objprop.setDireccion(aux + this.obtenerUbigeo(pn.getUbigeo()));
            aux = pn.getNumTitu();
            if ((validaNulo(aux)) || (aux.equals("00000000"))) {
                aux = aux
                        + (((pn.getNumTitu() == null) || (pn.getNumTitu()
                        .equals("0000"))) ? "" : (" - " + pn
                        .getAnoTitu()));
                objprop.setNumtitulo(aux);
            }
            ccont++;
            if (ccont == 1) {
                if (pn.getFecProp() != null) {
                    fechapro = this.formatDate(pn.getFecProp());
                }
            }
            objprop.setFechaProp(fechapro);

            aux = pn.getNumDocumento();
            if (aux != null && !aux.trim().equals("")) {
                objprop.setDocumentos(aux);
            } else {
                objprop.setDocumentos("########");
            }

            aux=pn.getTipoDocumento();
            if (aux != null && !aux.trim().equals("")) {
                objprop.setTipoDocumento(aux);
            } else {
                objprop.setTipoDocumento("########");
            }

            objprop.setTipoPartic("N");

            listaprop.add(objprop);
        }

        // Propietario Persona Juridica
        int j = 0;
        for (ParticipantesPjBean pj : listPj) {
            j++;
            ParticipantesBean objprop = new ParticipantesBean();
            StringBuilder concat = new StringBuilder();
            concat.append(String.valueOf(j)).append(". ");
            if (validaNulo(pj.getRazonSocial())) {
                concat.append(pj.getRazonSocial());
            } else {
                concat.append("");
            }
            objprop.setPropietario(concat.toString());
            aux = pj.getUbigeo();
            if (validaNulo(aux))
                objprop.setUbigeo(aux);
            aux = pj.getDireccion();
            if (validaNulo(aux))
                objprop.setDireccion(aux + this.obtenerUbigeo(pj.getUbigeo()));

            aux = pj.getNumTitu();
            if ((validaNulo(aux)) || (aux.equals("00000000"))) {
                aux = aux
                        + (((pj.getNumTitu() == null) || (pj.getNumTitu()
                        .equals("0000"))) ? "" : (" - " + pj
                        .getAnoTitu()));
                objprop.setNumtitulo(aux);
            }
            ccont++;
            if (ccont == 1) {
                if (pj.getFecProp() != null) {
                    fechapro = this.formatDate(pj.getFecProp());
                }
            }
            objprop.setFechaProp(fechapro);

            aux = pj.getNumDocumento();
            if (aux != null && !aux.trim().equals("")) {
                objprop.setDocumentos(aux);
            } else {
                objprop.setDocumentos("########");
            }

            aux=pj.getTipoDocumento();
            if (aux != null && !aux.trim().equals("")) {
                objprop.setTipoDocumento(aux);
            } else {
                objprop.setTipoDocumento("########");
            }

            objprop.setTipoPartic("J");
            listaprop.add(objprop);
        }

        return listaprop;
    }

    private String obtenerUbigeo(String ubigeo) {
        String rep = "";
        try {
            if(ubigeo.equalsIgnoreCase(" ")
                    || ubigeo == null
                    || ubigeo.trim().equals("")
                    || ubigeo.length() != 6){
                rep = " ";
            } else {
                String dpto = ubigeo.substring(0, 2);
                String prov = ubigeo.substring(2, 4);
                String dist = ubigeo.substring(4, 6);
                List<UbigeoBean> lista = consultaVehicularRepository.getNombreUbigeo(dpto, prov, dist);

                if(lista.isEmpty()){
                    rep = " ";
                } else {
                    for(UbigeoBean key : lista){
                        rep = " " + key.getDpto() + " - " + key.getProv() + " - " + key.getDist();
                    }
                }
            }
        } catch (Exception e) {
            rep = " ";
        }
        return rep;
    }

    private int guardarTransaccionBoletaInformativa(DatosBoletaRPV datosBoletaRPV, UsuarioJtiBean usuario) throws ExceptionBean {
        CuentaBean cuenta = consultaVehicularRepository.getDatosCuenta(datosBoletaRPV.getUsuario());
        LOG.info("cuenta.getLineaPrepagoId() = " + cuenta.getLineaPrepagoId());
        int transId = this.guardarTxBoletaInformativa(
                datosBoletaRPV.getIp(),
                datosBoletaRPV.getDatosBoletaRPV().getRegPubId(), datosBoletaRPV.getDatosBoletaRPV().getOficRegId(),
                String.valueOf(cuenta.getCuentaId()),
                datosBoletaRPV.getDatosBoletaRPV().getNumPlaca().trim(),
                cuenta.getLineaPrepagoId(),
                usuario.getUserSessionId(),
                datosBoletaRPV.getUsuario(),
                usuario, datosBoletaRPV);
        return transId;
    }

    private int guardarTxBoletaInformativa(String ip, String zona, String oficina,
                                                    String cuentaId, String numPart, Long lineaPrepagoId,
                                                    String userSessionId, String userId,
                                                    UsuarioJtiBean usuario,
                                                    DatosBoletaRPV datosBoletaRPV) throws ExceptionBean {
        TransaccionBean trans = new TransaccionBean();
        int codServicio = 0;
        StringBuffer strBusq = new StringBuffer();

        LOG.info("getOficinaRegistral " );
        OficinaRegistralBean oficinaReg = consultaVehicularRepository.getOficinaRegistral(zona, oficina);
        codServicio = SERVICIO_BOLETA_INFORMATIVA_RPV;
        strBusq.append("BOLETA INFORMATIVA VEHICULAR-")
                .append(" Ofic Reg: ").append(oficinaReg.getNombre()).append(" ")
                .append("NUMERO DE PLACA: ").append(numPart);

        BigDecimal costoServicio = new BigDecimal("0.0");
        LOG.info("getTarifaOficina " );
        TarifaBean tarifa = consultaVehicularRepository.getTarifaOficina("6", (long) codServicio);

        if (tarifa != null) {
            costoServicio = tarifa.getPrecOfic();
        }

        trans.setServicioId(codServicio);
        trans.setCodGrupoLibroArea(new BigDecimal("6"));
        trans.setCuentaId(Long.parseLong(cuentaId));
        trans.setCosto(costoServicio);
        trans.setIp(ip);
        trans.setSessionId(userSessionId);
        trans.setTipoUsr(FLG_1);// tipoUser 0= Interno y 1 = externo
        trans.setStrBusq(strBusq.toString());
        trans.setUserKeyId(usuario.getUserKeyId());
        trans.setBlobJson(new Gson().toJson(datosBoletaRPV, DatosBoletaRPV.class).getBytes());
        trans.setOficRegId(oficina);
        trans.setRegPubId(zona);
        LOG.info("saveTransaccion " );

        long idTrans = consultaVehicularRepository.saveTransaccion(trans);
        LOG.info("lineaPrepagoId = " + lineaPrepagoId);
        LineaPrepagoBean lineaPrepago = consultaVehicularRepository.obtenerLineaPrepago(lineaPrepagoId);
        lineaPrepago.setUsrUltModif(userId);
        lineaPrepago.setFgDeposito("0");
        LOG.info("actualizarSaldo " );
        consultaVehicularRepository.actualizarSaldo(lineaPrepago);

        MovimientoBean movimientoBean = MovimientoBean.builder()
                .tpoMov(FLG_1)
                .fecHor(LocalDateTime.now())
                .fgAsig(FLG_0)
                .lineaPrepago(lineaPrepagoId)
                .montoFin(lineaPrepago.getSaldo())
                .build();

        LOG.info("saveMovimiento " );
        long movimientoId = consultaVehicularRepository.saveMovimiento(movimientoBean);

        ConsumoBean consumo = new ConsumoBean();
        consumo.setMonto(costoServicio);
        consumo.setMovimientoId(movimientoId);
        consumo.setTpoConsumo("U");
        consumo.setTransId(idTrans);
        LOG.info("saveConsumo " );
        consultaVehicularRepository.saveConsumo(consumo);

        LOG.info("getPersonaIdByLineaPrepagoId " );
        long personaId = consultaVehicularRepository.getPersonaIdByLineaPrepagoId(lineaPrepagoId);

        AbonoBean abono = AbonoBean.builder()
                .tipoAbono("L")
                .tipoVent("P")
                .usrCaja(userId)
                .tipoUsr("I")
                .monto(costoServicio)
                .movimientoId(movimientoId)
                .oficRegId("00")
                .regPubId("00")
                .fgCierre(FLG_0)
                .personaId(personaId)
                .tsCrea(LocalDateTime.now())
                .tsModi(LocalDateTime.now())
                .estado(FLG_1)
                .build();
        LOG.info("saveAbono " );
        long abonoId = consultaVehicularRepository.saveAbono(abono);

        ComprobanteBean comprobanteBean = ComprobanteBean.builder()
                .abonoId(abonoId)
                .monto(costoServicio)
                .estado(FLG_1)
                .build();
        LOG.info("saveComprobante " );
        consultaVehicularRepository.saveComprobante(comprobanteBean);
        return (int) idTrans;
    }

    private void enviarCorreo(RegularizaPagoBean regularizaPagoBean, ReciboPublBean resp) throws ExceptionBean {
        String mensaje = "";
        try {
            mensaje = buildMessage(regularizaPagoBean, resp,"B");
            String appName = "App Sunarp";
            // Email email = new Email();
            int rpt = email.sendEmail(
                    new String[]{
                            resp.getEmail()
                    }
                    ,appName + " - Pago de Servicio"
                    ,mensaje, null, false, false, null, new String[]{"app@sunarp.gob.pe"}, null, appName);

        } catch(Exception e){
            log.error("Ocurrio un error al enviar el mensaje: {}", e.getLocalizedMessage());
        }
    }

    private String buildMessage(RegularizaPagoBean datos, ReciboPublBean resp, String tipoServicio) throws Exception{
        //-- BODY
        StringBuffer sb = new StringBuffer();
        BufferedReader br = new BufferedReader(
                new InputStreamReader(getClass().getResourceAsStream("/pages/recibo.html"), "UTF-8")
        );
        for (int c = br.read(); c != -1; c = br.read()) sb.append((char)c);
        String body = sb.toString().trim();

        if (tipoServicio.equals("B")){
            StringBuffer aviso =  new StringBuffer("<tr class='ui-widget-content' role='row'><td role='gridcell'><br/><label class='ui-outputlabel'>IMPORTANTE:<br/>"
                    + "La boleta informativa no es enviada por correo electr&oacute;nico.<br/>Para ver o descargar este documento debe ingresar a 'Mi Perfil' y luego 'Ver historial'.<br/>"
                    + "Si usa Android, debe verificar los permisos de nuestra App en su dispositivo y adicionalmente se le sugiere tener instalada una aplicaci&oacute;n para abrir archivos PDF."
                    + "</label></td></tr>");
            body = body.replaceAll("###_TIPOSERV_###", BOLETA_INFORMATIVA);
            body = body.replaceAll("###_TEXTOCOMPENDIOSO_###", "");
            body = body.replaceAll("###_AVISO_###", aviso.toString());
        }

        body = body.replaceAll("###_TRANSACCION_###", String.valueOf(datos.getTransId()));
        body = body.replaceAll("###_SERVICIO_###", resp.getDescripcion());
        body = body.replaceAll("###_FECHA_###", resp.getTsCrea());
        body = body.replaceAll("###_MONTO_###", resp.getTotal().toString());
        body = body.replaceAll("###_MAIL_###", resp.getEmail());
        body = body.replaceAll("###_NOMBRE_###", resp.getSolicitante());

        return body;
    }

    private byte[] readAllBytes(InputStream inputStream) throws IOException {
        final int bufLen = 4 * 0x400; // 4KB
        byte[] buf = new byte[bufLen];
        int readLen;
        IOException exception = null;

        try {
            try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream()) {
                while ((readLen = inputStream.read(buf, 0, bufLen)) != -1)
                    outputStream.write(buf, 0, readLen);

                return outputStream.toByteArray();
            }
        } catch (IOException e) {
            exception = e;
            throw e;
        } finally {
            if (exception == null) inputStream.close();
            else try {
                inputStream.close();
            } catch (IOException e) {
                exception.addSuppressed(e);
            }
        }
    }

    private String[] ordenarPropietarios(List<PropietarioBean> propietarios) {
        StringBuilder prop = new StringBuilder();
        String sep = "X_X";
        if(!propietarios.isEmpty()){
            int max = applicationProperties.getMaxCharline();
            StringBuilder line;
            StringBuilder newLine;
            for(PropietarioBean propietario : propietarios){
                line = new StringBuilder();
                newLine = new StringBuilder();
                String[] words = propietario.getTitular().trim().split(" ");
                for(String word : words){
                    if(newLine.length() + word.length() + 1 < max){
                        line.append(word).append(" ");
                        newLine.append(word).append(" ");
                    } else {
                        newLine.delete(0, newLine.length() - 1);
                        line.append(sep).append(" ").append(word).append(" ");
                        newLine.append(word).append(" ");
                    }
                }
                prop.append(line).append(sep);
            }
        }
        return prop.toString().split(sep);
    }

    private String getTodayDate(){
        LocalDate dateNow = LocalDate.now();
        return dateNow.getYear()
                + String.format("%02d", dateNow.getMonthValue())
                + String.format("%02d", dateNow.getDayOfMonth());
    }

    private String getTodayDateTime(){
        LocalDateTime dateNow = LocalDateTime.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TS_DATE_DMY);
        return dateNow.format(formatter);
    }

    private String formatDate(String fecha){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(BD_DATE);
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern(DET_VEH);
        LocalDateTime date = LocalDateTime.parse(fecha, formatter);
        return date.format(formatter2);
    }

    private String formatDateTimesTamp(Date pfecha) {
        SimpleDateFormat format = new SimpleDateFormat(TS_DATE_YMD);
        return format.format(pfecha);
    }

    private boolean validaNulo(String cadena) {
        return !(cadena == null || cadena.trim().equals(""));
    }

    public String convertPlateNumber(String strPlaca){
        StringBuilder strCadena = new StringBuilder();
        for (int i = 0; i < strPlaca.length(); i++) {
            char chrs = strPlaca.charAt(i);
            if (Character.isLetterOrDigit(chrs))
                strCadena.append(chrs);
        }
        return String.format("%-7s", strCadena);
    }
    
    @Override
	@Async
    public CompletableFuture<Boolean> asyncGenerarBoleta(GenerarBoletaRequest generarBoletaRequest, 
    				String guid, MultiValueMap<String, String> headers) throws ExceptionBean {
        LOG.info("asyncGenerarBoleta - CompletableFuture");

        RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
	    restTemplate = restTemplateBuilder.build();
	    String urlBase = context.getEnvironment().getProperty(Constantes.URL_GENERAR_BOLETA);
	    String urlContexto = context.getEnvironment().getProperty(Constantes.URL_CONTEXT_PATH);
	    
	    String url = String.format(urlBase + urlContexto + "/consulta-vehicular/async/generarBoleta/" + guid);
	    HttpEntity<GenerarBoletaRequest> requestHttp = new HttpEntity<>(generarBoletaRequest, headers);
	    
	    LOG.info("url = " + url);
	    restTemplate.postForObject(url, requestHttp, String.class);

        return CompletableFuture.completedFuture(true);  
    }
    
    @Override
    public Boolean finAsyncGenerarBoleta(String guid, GenerarBoletaResponse resultado)  throws ExceptionBean{
    	LOG.info("finAsyncGenerarBoleta: " + guid);
    	BoletaVehicularRedis data = redisService.getBoletaVehicularRedis(guid); 
        data.setGuid(guid);
    	data.setTransId(resultado.getTransId());
	    data.setStatus("1");
	    redisService.saveBoletaVehicularRedis(data);
		
		return true;
    }
    
}
