package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ReciboPublBean {
    private long solicitudId;
    private String descripcion;
    private String tsCrea;
    private BigDecimal total;
    private String email;
    private String solicitante;
    private String tpoPago;
    private long pagoSolicitudId;
    private String numeroRecibo;
    private String numeroPublicidad;
    private String codVerificacion;

    private long secReciDetaAtenNac;
}
