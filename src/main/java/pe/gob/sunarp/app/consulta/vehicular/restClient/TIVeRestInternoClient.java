package pe.gob.sunarp.app.consulta.vehicular.restClient;

import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeInternoRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeInternoResponse;

public interface TIVeRestInternoClient {
	TIVeInternoResponse processTIVeInterno(TIVeInternoRequest tiveRequest) throws ExceptionBean ;

}
