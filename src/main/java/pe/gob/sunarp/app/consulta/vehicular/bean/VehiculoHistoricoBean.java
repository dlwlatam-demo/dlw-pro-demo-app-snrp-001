package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VehiculoHistoricoBean {
    private String numPlaca;
    private long nsPlaca;
    private String numMotor;
    private String numSerie;

    private String nsPlac;
}
