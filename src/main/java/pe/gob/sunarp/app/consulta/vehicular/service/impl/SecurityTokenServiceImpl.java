package pe.gob.sunarp.app.consulta.vehicular.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.sunarp.app.consulta.vehicular.cache.RedisService;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionProperties;
import pe.gob.sunarp.app.consulta.vehicular.service.SecurityTokenService;

@Service
public class SecurityTokenServiceImpl implements SecurityTokenService {

    @Autowired
    private RedisService redis;

    @Autowired
    private ExceptionProperties exceptionProperties;

    @Override
    public void validateToken(String codigo) throws ExceptionBean {
        Integer valor = redis.getTokenCache(codigo);

        if (valor == null){
            throw exceptionProperties.getUnauthorized();
        }
    }
}
