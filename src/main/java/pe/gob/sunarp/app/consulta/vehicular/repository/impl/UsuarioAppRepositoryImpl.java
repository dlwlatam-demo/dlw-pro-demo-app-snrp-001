package pe.gob.sunarp.app.consulta.vehicular.repository.impl;

import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import pe.gob.sunarp.app.consulta.vehicular.bean.LineaPrepagoBean;
import pe.gob.sunarp.app.consulta.vehicular.bean.TaAppsunarpLogBean;
import pe.gob.sunarp.app.consulta.vehicular.bean.UsuarioAppBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.repository.UsuarioAppRepository;
import pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper.LineaPrepagoRowMapper;
import pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper.UsuarioAppMapper;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.*;

@Repository
public class UsuarioAppRepositoryImpl extends JdbcDaoSupport implements UsuarioAppRepository {

    private static final Logger LOG = LoggerFactory.getLogger(UsuarioAppRepositoryImpl.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    public void DataSource(DataSource dataSource) {
        setDataSource(dataSource);
    }
	
	@Override
	public void guardarAppLog(TaAppsunarpLogBean appLog)  throws ExceptionBean {
        LOG.info("Inicio guardarAppLog.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_TA_APPSUNARP_LOG)
                .withProcedureName(SP_NUEV_TA_APPSUNARP_LOG)
                .declareParameters(
        				new SqlParameter("in_VISANETRESPONSE", OracleTypes.BINARY),
        				new SqlOutParameter("pov_valid_operation", OracleTypes.NUMBER));
		
        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("piv_ip_address", appLog.getIpAddress())
                .addValue("pic_id_usua_crea", appLog.getIdUsuarioCrea())
                .addValue("piv_user_key_id", appLog.getUserKeyId())
                .addValue("piv_ws_method", appLog.getWsMethod())
                .addValue("in_VISANETRESPONSE", appLog.getJsonRequest().getBytes());

        Map<String, Object>  outParameters = simpleJdbcCall.execute(inputParameters);

		BigDecimal previo = (BigDecimal) outParameters.get("pov_valid_operation");
		LOG.info("resultado: " + previo);
	}

	/*
    @Override
    public UsuarioAppBean getUserByKeyId(String userKeyId) throws ExceptionBean {
        LOG.info("Inicio getUserByKeyId.");
        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_TA_USER_APP)
                .withProcedureName(SP_OBTN_TA_USER_APP_BY_USERKEY )
                .declareParameters(new SqlOutParameter("pocur", OracleTypes.CURSOR, new UsuarioAppMapper()),
                        new SqlParameter("piv_userKeyId", OracleTypes.VARCHAR));

        SqlParameterSource	 inputParameters = new MapSqlParameterSource()
                .addValue("piv_userKeyId", userKeyId);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<UsuarioAppBean> list = (List<UsuarioAppBean>) outParameters.get("pocur");

        if (list.isEmpty()) {
            return null;
        }
        return list.get(0);
    }
    */

}
