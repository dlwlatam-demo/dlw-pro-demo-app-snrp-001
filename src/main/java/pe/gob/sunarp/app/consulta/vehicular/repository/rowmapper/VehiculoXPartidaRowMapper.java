package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoBean;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoPartidaBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiculoXPartidaRowMapper implements RowMapper<VehiculoPartidaBean> {

    @Override
    public VehiculoPartidaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return VehiculoPartidaBean.builder()
                .numPlaca(rs.getString("NUM_PLACA"))
                .codModelo(rs.getString("COD_MODELO"))
                .descModelo(rs.getString("DESC_MODELO"))
                .codMarca(rs.getString("COD_MARCA"))
                .descMarca(rs.getString("DESC_MARCA"))
                .codCondVeh(rs.getString("COD_COND_VEHI"))
                .descCondVeh(rs.getString("DESC_COND_VEHI"))
                .codTipoVeh(rs.getString("COD_TIPO_VEHI"))
                .descTipoVeh(rs.getString("DESC_TIPO_VEHI"))
                .codTipoComb(rs.getString("COD_TIPO_COMB"))
                .descTipoComb(rs.getString("DESC_TIPO_COMB"))
                .codColorOrig(rs.getString("COD_COLOR_01_ORIG"))
                .color01(rs.getString("DESC_COLOR_01"))
                .color02(rs.getString("DESC_COLOR_02"))
                .color03(rs.getString("DESC_COLOR_03"))
                .anoFab(rs.getString("ANO_FABRIC"))
                .numSerie(rs.getString("NUM_SERIE"))
                .numMotor(rs.getString("NUM_MOTOR"))
                .numCilindros(rs.getString("NUM_CILINDROS"))
                .pesoSeco(rs.getString("PESO_SECO"))
                .pesoBruto(rs.getString("PESO_BRUTO"))
                .numPasajeros(rs.getString("NUM_PASAJEROS"))
                .numAsientos(rs.getString("NUM_ASIENTOS"))
                .numEjes(rs.getString("NUM_EJES"))
                .numRuedas(rs.getString("NUM_RUEDAS"))
                .numPuertas(rs.getString("NUM_PUERTAS"))
                .longitud(rs.getString("LONGITUD"))
                .ancho(rs.getString("ANCHO"))
                .altura(rs.getString("ALTURA"))
                .tsInscrip(rs.getString("TS_INSCRIP"))
                .codTipoCarr(rs.getString("COD_TIPO_CARR"))
                .descTipoCarr(rs.getString("DESC_TIPO_CARR"))
                .fgBaja(rs.getString("FG_BAJA"))
                .refNumPart(rs.getString("REFNUM_PART"))
                .numPartida(rs.getString("NUM_PARTIDA"))
                .descCetico(rs.getString("CETICO_DESC"))
                .nomOficina(rs.getString("NOMBRE"))
                .oficRegId(rs.getString("OFIC_REG_ID"))
                .areaRegId(rs.getString("AREA_REG_ID"))
                .regPubId(rs.getString("REG_PUB_ID"))
                .noVin(rs.getString("NO_VIN"))
                .coCateg(rs.getString("CO_CATG"))
                .anMode(rs.getString("AN_MODE"))
                .noVers(rs.getString("NO_VERS"))
                .poMotr(rs.getString("PO_MOTR"))
                .noClda(rs.getString("NO_CLDA"))
                .noForm(rs.getString("NO_FORM"))
                .coTipoUsos(rs.getString("CO_TIPO_USOS"))
                .build();
    }
}