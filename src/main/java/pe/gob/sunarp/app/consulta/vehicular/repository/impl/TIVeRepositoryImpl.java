package pe.gob.sunarp.app.consulta.vehicular.repository.impl;

import oracle.jdbc.OracleTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import pe.gob.sunarp.app.consulta.vehicular.bean.TIVeBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionProperties;
import pe.gob.sunarp.app.consulta.vehicular.repository.TIVeRepository;
import pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper.TIVeRowMapper;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeRequest;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.*;

@Repository
public class TIVeRepositoryImpl implements TIVeRepository{
    private static final Logger LOG = LoggerFactory.getLogger(TIVeRepositoryImpl.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ExceptionProperties exceptionProperties;

	@Override
	public List<TIVeBean> getTIVeUserHistory(Long userId)  throws ExceptionBean {
       LOG.info("Inicio getTIVeUserHistory.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_TA_USER_TIVE)
                .withProcedureName(SEL_TA_USER_TIVE)
                .declareParameters(new SqlOutParameter("pocur", OracleTypes.CURSOR, new TIVeRowMapper()),
                        new SqlParameter("in_ID_USER", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("in_ID_USER", userId);

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        List<TIVeBean> list = (List<TIVeBean>) outParameters.get("pocur");

        if(list.isEmpty()) {
            throw exceptionProperties.getNotFound();
        }

        LOG.info("resultado getTIVeUserHistory = " + list.size());
        return list;
	}

    @Override
    public int countTIVeHistory(Long userId, TIVeRequest tiveRequest) throws ExceptionBean {
        LOG.info("Inicio countTIVeHistory.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_VEHICULAR)
                .withProcedureName(COUNT_TA_USER_TIVE)
                .declareParameters( new SqlParameter("in_ID_USER", OracleTypes.NUMBER),
                    new SqlParameter("in_REG_PUB_ID", OracleTypes.CHAR),
                    new SqlParameter("in_OFIC_REG_ID", OracleTypes.CHAR),
                    new SqlParameter("in_AA_TITU", OracleTypes.CHAR),
                    new SqlParameter("in_NUM_TITU", OracleTypes.CHAR),
                    new SqlParameter("in_NUM_PLACA", OracleTypes.CHAR),
                    new SqlParameter("in_COD_VERIFICACION", OracleTypes.VARCHAR),
                    new SqlParameter("in_TIPO", OracleTypes.CHAR),
                    new SqlOutParameter("out_count", OracleTypes.NUMBER));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("in_ID_USER", userId)
                .addValue("in_REG_PUB_ID", tiveRequest.getCodigoZona())
                .addValue("in_OFIC_REG_ID", tiveRequest.getCodigoOficina())
                .addValue("in_AA_TITU", tiveRequest.getAnioTitulo())
                .addValue("in_NUM_TITU", tiveRequest.getNumeroTitulo())
                .addValue("in_NUM_PLACA", tiveRequest.getNumeroPlaca())
                .addValue("in_COD_VERIFICACION", tiveRequest.getCodigoVerificacion())
                .addValue("in_TIPO", tiveRequest.getTipo());

        Map<String, Object> outParameters = simpleJdbcCall.execute(inputParameters);
        BigDecimal count = (BigDecimal) outParameters.get("out_count");

        LOG.info("resultado countTIVeHistory = " + count);
        return count.intValue();
    }

    @Override
    public void saveTIVeHistory(Long userId, TIVeRequest tiveRequest) throws ExceptionBean {
        LOG.info("Inicio saveTIVeHistory.");

        JdbcTemplate jdbcTemplate = context.getBean(JDBC, JdbcTemplate.class);

        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName(APPSUNARP)
                .withCatalogName(PKG_TA_USER_TIVE)
                .withProcedureName(INS_TA_USER_TIVE)
                .declareParameters( new SqlParameter("in_ID_USER", OracleTypes.NUMBER),
                        new SqlParameter("in_REG_PUB_ID", OracleTypes.CHAR),
                        new SqlParameter("in_OFIC_REG_ID", OracleTypes.CHAR),
                        new SqlParameter("in_AA_TITU", OracleTypes.CHAR),
                        new SqlParameter("in_NUM_TITU", OracleTypes.CHAR),
                        new SqlParameter("in_NUM_PLACA", OracleTypes.CHAR),
                        new SqlParameter("in_CCOD_VERIFICACION", OracleTypes.VARCHAR),
                        new SqlParameter("in_TIPO", OracleTypes.CHAR));

        SqlParameterSource inputParameters = new MapSqlParameterSource()
                .addValue("in_ID_USER", userId)
                .addValue("in_REG_PUB_ID", tiveRequest.getCodigoZona())
                .addValue("in_OFIC_REG_ID", tiveRequest.getCodigoOficina())
                .addValue("in_AA_TITU", tiveRequest.getAnioTitulo())
                .addValue("in_NUM_TITU", tiveRequest.getNumeroTitulo())
                .addValue("in_NUM_PLACA", tiveRequest.getNumeroPlaca())
                .addValue("in_CCOD_VERIFICACION", tiveRequest.getCodigoVerificacion())
                .addValue("in_TIPO", tiveRequest.getTipo());

        simpleJdbcCall.execute(inputParameters);

        LOG.info("resultado saveTIVeHistory = " + 1);
    }

}
