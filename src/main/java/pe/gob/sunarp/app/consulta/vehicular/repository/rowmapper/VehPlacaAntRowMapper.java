package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoPlacaAnteriorBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehPlacaAntRowMapper implements RowMapper<VehiculoPlacaAnteriorBean> {

    @Override
    public VehiculoPlacaAnteriorBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        VehiculoPlacaAnteriorBean item = new VehiculoPlacaAnteriorBean();
        item.setNumPlaca(rs.getString("numPlaca"));
        return item;
    }
}
