package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoHistoricoBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehHistoricoRowMapper implements RowMapper<VehiculoHistoricoBean> {

    @Override
    public VehiculoHistoricoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        VehiculoHistoricoBean item = new VehiculoHistoricoBean();
        item.setNumPlaca(rs.getString("numPlaca"));
        item.setNsPlaca(rs.getLong("nsPlaca"));
        item.setNumMotor(rs.getString("numMotor"));
        item.setNumSerie(rs.getString("numSerie"));
        return item;
    }
}
