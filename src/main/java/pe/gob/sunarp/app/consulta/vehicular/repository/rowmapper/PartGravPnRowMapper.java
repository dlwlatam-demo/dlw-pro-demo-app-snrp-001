package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.ParticipantesPnBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PartGravPnRowMapper implements RowMapper<ParticipantesPnBean> {

    @Override
    public ParticipantesPnBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ParticipantesPnBean.builder()
                .apePaterno(rs.getString("apePat"))
                .apeMaterno(rs.getString("apeMat"))
                .nombres(rs.getString("nombres"))
                .desPart(rs.getString("desPart"))
                .tipoPers(rs.getString("tipoPers"))
                .build();
    }
}