package pe.gob.sunarp.app.consulta.vehicular.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pe.gob.sunarp.app.consulta.vehicular.config.Auth;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.BuscarTIVeResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.ProcesoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeResponse;
import pe.gob.sunarp.app.consulta.vehicular.service.ConsultaTiveService;
import pe.gob.sunarp.app.consulta.vehicular.service.SecurityTokenService;

import java.util.List;

@Slf4j
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/consulta-tive")
public class ConsultaTiveRest {

    @Autowired
    private Auth auth;

    @Autowired
    private SecurityTokenService securityTokenService;

    @Autowired
    private ConsultaTiveService consultaTiveService;

    @PostMapping(value = "/busqueda")
    public ResponseEntity<BuscarTIVeResponse> buscarTive(@RequestBody TIVeRequest tiveRequest) throws ExceptionBean {
        securityTokenService.validateToken(auth.usuario().getJti());
        return new ResponseEntity<>(consultaTiveService.buscarTive(tiveRequest), HttpStatus.OK);
    }

    @GetMapping(value = "/historial/busqueda")
    public ResponseEntity<List<TIVeResponse>> buscarHistorial() throws ExceptionBean {
        securityTokenService.validateToken(auth.usuario().getJti());
        return new ResponseEntity<>(consultaTiveService.buscarHistorial(auth.usuario().getJti()),HttpStatus.OK);
    }

    @PostMapping(value = "/historial/insertar")
    public ResponseEntity<ProcesoResponse> insertarHistorial(@RequestBody TIVeRequest tiveRequest) throws ExceptionBean {
        securityTokenService.validateToken(auth.usuario().getJti());
        return new ResponseEntity<>(consultaTiveService.insertarHistorial(auth.usuario().getJti(), tiveRequest), HttpStatus.OK);
    }
}
