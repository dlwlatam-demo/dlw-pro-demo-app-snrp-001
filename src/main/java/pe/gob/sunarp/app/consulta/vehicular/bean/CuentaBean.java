package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CuentaBean {
    private Long cuentaId;
    private Long lineaPrepagoId;
}
