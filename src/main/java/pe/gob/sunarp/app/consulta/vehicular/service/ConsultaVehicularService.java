package pe.gob.sunarp.app.consulta.vehicular.service;

import java.util.concurrent.CompletableFuture;

import org.springframework.util.MultiValueMap;

import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.request.GenerarBoletaRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.GetDatosVehiculoRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.ValidarBoletaRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.GenerarBoletaResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.GetDatosVehiculoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.ProcesoResponse;

public interface ConsultaVehicularService {
    GetDatosVehiculoResponse getDatosVehiculo(GetDatosVehiculoRequest getDatosVehiculoRequest, String jti) throws ExceptionBean;
    ProcesoResponse validarBoleta(ValidarBoletaRequest validarBoletaRequest) throws ExceptionBean;
    GenerarBoletaResponse generarBoleta(GenerarBoletaRequest generarBoletaRequest, String jti) throws ExceptionBean;
    
    CompletableFuture<Boolean> asyncGenerarBoleta(GenerarBoletaRequest generarBoletaRequest, 
			String guid, MultiValueMap<String, String> headers) throws ExceptionBean;
    Boolean finAsyncGenerarBoleta(String guid, GenerarBoletaResponse resultado)  throws ExceptionBean;
}
