package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.UsuarioAppBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UsuarioAppMapper implements RowMapper<UsuarioAppBean> {

    @Override
    public UsuarioAppBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        UsuarioAppBean usuarioApppBean  = new UsuarioAppBean();
        usuarioApppBean.setTipo(rs.getString("tipo"));
        usuarioApppBean.setIdUser(rs.getLong("id_user"));
        usuarioApppBean.setEmail(rs.getString("email"));
        usuarioApppBean.setSexo(rs.getString("sexo"));
        usuarioApppBean.setTipoDoc(rs.getString("tipo_doc"));
        usuarioApppBean.setNroDoc(rs.getString("nro_doc"));
        usuarioApppBean.setNombres(rs.getString("nombres"));
        usuarioApppBean.setPriApe(rs.getString("pri_ape"));
        usuarioApppBean.setSegApe(rs.getString("seg_ape"));
        usuarioApppBean.setFecNac(rs.getDate("fec_nac"));
        usuarioApppBean.setNroCelular(rs.getString("nro_celular"));
        usuarioApppBean.setStatus(rs.getString("status"));
        usuarioApppBean.setPassword(rs.getString("password"));
        usuarioApppBean.setRememberToken(rs.getString("remember_token"));
        usuarioApppBean.setCreatedAt(rs.getDate("created_at"));
        usuarioApppBean.setUpdatedAt(rs.getDate("updated_at"));
        usuarioApppBean.setDeletedAt(rs.getDate("deleted_at"));
        usuarioApppBean.setAppVersion(rs.getString("app_version"));
        usuarioApppBean.setIpAddress(rs.getString("ip_address"));
        usuarioApppBean.setCodValidacion(rs.getString("cod_validacion"));
        usuarioApppBean.setIdUsuaCrea(rs.getString("id_usua_crea"));
        usuarioApppBean.setIdUsuaModi(rs.getString("id_usua_modi"));
        usuarioApppBean.setLastConn(rs.getDate("last_conn"));
        usuarioApppBean.setGeoLat(rs.getDouble("geo_lat"));
        usuarioApppBean.setGeoLong(rs.getDouble("geo_long"));
        usuarioApppBean.setUserKeyId(rs.getString("user_key_id"));
        usuarioApppBean.setUserPhoto(rs.getBytes("user_photo"));
        return usuarioApppBean;
    }
}
