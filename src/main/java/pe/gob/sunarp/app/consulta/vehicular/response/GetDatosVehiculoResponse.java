package pe.gob.sunarp.app.consulta.vehicular.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetDatosVehiculoResponse {
    private String datosVehiculo;
}
