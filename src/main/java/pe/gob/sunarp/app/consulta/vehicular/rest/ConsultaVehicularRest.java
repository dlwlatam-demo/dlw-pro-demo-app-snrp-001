package pe.gob.sunarp.app.consulta.vehicular.rest;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import pe.gob.sunarp.app.consulta.vehicular.bean.BoletaVehicularRedis;
import pe.gob.sunarp.app.consulta.vehicular.cache.RedisService;
import pe.gob.sunarp.app.consulta.vehicular.config.Auth;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.request.GenerarBoletaRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.GetDatosVehiculoRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.ValidarBoletaRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.GenerarBoletaResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.GetDatosVehiculoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.ProcesoResponse;
import pe.gob.sunarp.app.consulta.vehicular.service.ConsultaVehicularService;
import pe.gob.sunarp.app.consulta.vehicular.service.SecurityTokenService;
import pe.gob.sunarp.app.seguridad.bean.UsuarioJtiBean;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;

import jakarta.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/consulta-vehicular")
public class ConsultaVehicularRest {
	private static final Logger LOG = LoggerFactory.getLogger(ConsultaVehicularRest.class);

    @Autowired
    private Auth auth;

    @Autowired
    private SecurityTokenService securityTokenService;

    @Autowired
    private ConsultaVehicularService consultaVehicularService;
    
    @Autowired
    private RedisService redisService;

    @PostMapping(value = "/vehiculo/data")
    public ResponseEntity<GetDatosVehiculoResponse> getDatosVehiculo(@RequestBody GetDatosVehiculoRequest getDatosVehiculoRequest,
    		HttpServletRequest request) throws ExceptionBean {
        securityTokenService.validateToken(auth.usuario().getJti());

        String ip="";
     	try {
 			ip = request.getRemoteAddr();
 		} catch (Exception e) {
 			ip = "0:0:0:0:0:0:0:1";
 		}		
     	getDatosVehiculoRequest.setIpAddress(ip);
        return new ResponseEntity<>(consultaVehicularService.getDatosVehiculo(getDatosVehiculoRequest, auth.usuario().getJti()), HttpStatus.OK);
    }

    @PostMapping(value = "/boleta/validar")
    public ResponseEntity<ProcesoResponse> validarBoleta(@RequestBody ValidarBoletaRequest validarBoletaRequest,
    		HttpServletRequest request) throws ExceptionBean {
        securityTokenService.validateToken(auth.usuario().getJti());

        UsuarioJtiBean usuario = redisService.getUsuarioJti(auth.usuario().getJti());
        String ip="";
     	try {
 			ip = request.getRemoteAddr();
 		} catch (Exception e) {
 			ip = "0:0:0:0:0:0:0:1";
 		}		
     	validarBoletaRequest.setIpAddress(ip);
        validarBoletaRequest.setUserKeyId(usuario.getUserKeyId());
        validarBoletaRequest.setUsuario("APPSNRPANDRO");
        validarBoletaRequest.setCorreo(usuario.getEmail());
        return new ResponseEntity<>(consultaVehicularService.validarBoleta(validarBoletaRequest),HttpStatus.OK);
    }

    @PostMapping(value = "/boleta/generar")
    public ResponseEntity<GenerarBoletaResponse> generarBoleta(@RequestBody GenerarBoletaRequest generarBoletaRequest) throws ExceptionBean {
        securityTokenService.validateToken(auth.usuario().getJti());
        return new ResponseEntity<>(consultaVehicularService.generarBoleta(generarBoletaRequest, auth.usuario().getJti()), HttpStatus.OK);
    }
    
    

    @PostMapping(value = "/boleta/generarAsync")
    public ResponseEntity<BoletaVehicularRedis> asyncGenerarBoleta(@RequestBody GenerarBoletaRequest generarBoletaRequest,
			HttpServletRequest request, @RequestHeader MultiValueMap<String, String> header) throws ExceptionBean {
    	securityTokenService.validateToken(auth.usuario().getJti());
		LOG.info("usuario = " + auth.usuario().getUserName());
        String ip="";
    	try {
			ip = request.getRemoteAddr();
		} catch (Exception e) {
			ip = "0:0:0:0:0:0:0:1";
		}
			
    	generarBoletaRequest.setIp(ip);

		UUID uuid = UUID.randomUUID();
		String sGuid = uuid.toString();
		BoletaVehicularRedis data = new BoletaVehicularRedis();
        
        data.setGuid(sGuid);
	    data.setStatus("0");
	    redisService.saveBoletaVehicularRedis(data);
	    BoletaVehicularRedis result1 = redisService.getBoletaVehicularRedis(sGuid);
	    if(result1 == null) {
	    	LOG.error("getGenerarBoletaRedis = NULL");
	    }
		this.consultaVehicularService.asyncGenerarBoleta(generarBoletaRequest, sGuid, header);
		
		LOG.info("asyncGenerarBoleta: Fin");
		return new ResponseEntity<>(data, HttpStatus.OK);
    }
    
    @PostMapping(value = "/async/generarBoleta/{guid}")
    public Callable<String> finAsyncGenerarBoleta(@RequestBody GenerarBoletaRequest generarBoletaRequest,
                                                                   @RequestHeader Map<String, String> header,
                                                                   @PathVariable String guid) throws ExceptionBean {
        LOG.info("finAsyncGenerarBoleta: Inicio");
    	securityTokenService.validateToken(auth.usuario().getJti());
		LOG.info("usuario = " + auth.usuario().getUserName());
		GenerarBoletaResponse resultado = consultaVehicularService.generarBoleta(generarBoletaRequest, 
        												auth.usuario().getJti());

		this.consultaVehicularService.finAsyncGenerarBoleta(guid, resultado);
		LOG.info("finAsyncGenerarBoleta: Fin");
		
        return  () ->("");
    }

    @GetMapping(value = "/async/status/generarBoleta/{guid}")
    public ResponseEntity<BoletaVehicularRedis> statusGenerarBoleta(@PathVariable String guid) throws ExceptionBean {
		LOG.info("statusGenerarBoleta: Inicio");

		BoletaVehicularRedis resultado = this.redisService.getBoletaVehicularRedis(guid);
		if(resultado.getStatus().equals("1")) {

			LOG.info("statusGenerarBoleta: ok");
		}
		LOG.info("statusGenerarBoleta: Fin");
		return new ResponseEntity<>(resultado, HttpStatus.OK);
		
    }
    
    
}
