package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.ParticipantesPnBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PropietarioHistoricoPnRowMapper implements RowMapper<ParticipantesPnBean> {

    @Override
    public ParticipantesPnBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ParticipantesPnBean.builder()
                .apePaterno(rs.getString("apePat"))
                .apeMaterno(rs.getString("apeMat"))
                .nombres(rs.getString("nombres"))
                .direccion(rs.getString("direccion"))
                .tipoPers(rs.getString("tipoPers"))
                .tipoDocumento(rs.getString("nombreAbrev"))
                .numDocumento(rs.getString("nuDocIden"))
                .build();
    }
}