package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.TitulosPendientesBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TitulosPendientesRowMapper implements RowMapper<TitulosPendientesBean> {

    @Override
    public TitulosPendientesBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        TitulosPendientesBean item = new TitulosPendientesBean();
        item.setAno(rs.getString("ANO_TITU"));
        item.setNumTitulo(rs.getString("NUM_TITU"));
        item.setRegPubId(rs.getString("REG_PUB_ID"));
        item.setOficRegId(rs.getString("OFIC_REG_ID"));
        item.setAreaRegistralId(rs.getString("AREA_REG_ID"));
        return item;
    }
}