package pe.gob.sunarp.app.consulta.vehicular.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import pe.gob.sunarp.app.consulta.vehicular.bean.DatosBoletaRPV;
import pe.gob.sunarp.app.consulta.vehicular.request.GenerarBoletaRequest;

@Mapper(componentModel = "spring" )
public interface DatosBoletaMapper {

    @Mapping(target = "idUser", source = "generarBoletaRequest.visanetResponse.idUser")
    @Mapping(target = "idUnico", source = "generarBoletaRequest.visanetResponse.idUnico")
    @Mapping(target = "pan", source = "generarBoletaRequest.visanetResponse.pan")
    @Mapping(target = "dscCodAccion", source = "generarBoletaRequest.visanetResponse.dscCodAccion")
    @Mapping(target = "codAutoriza", source = "generarBoletaRequest.visanetResponse.codAutoriza")
    @Mapping(target = "codtienda", source = "generarBoletaRequest.visanetResponse.codtienda")
    @Mapping(target = "numOrden", source = "generarBoletaRequest.visanetResponse.numOrden")
    @Mapping(target = "codAccion", source = "generarBoletaRequest.visanetResponse.codAccion")
    @Mapping(target = "fechaYhoraTx", source = "generarBoletaRequest.visanetResponse.fechaYhoraTx")
    @Mapping(target = "nomEmisor", source = "generarBoletaRequest.visanetResponse.nomEmisor")
    @Mapping(target = "oriTarjeta", source = "generarBoletaRequest.visanetResponse.oriTarjeta")
    @Mapping(target = "respuesta", source = "generarBoletaRequest.visanetResponse.respuesta")
    @Mapping(target = "transId", source = "generarBoletaRequest.visanetResponse.transId")
    @Mapping(target = "concepto", source = "generarBoletaRequest.visanetResponse.concepto")
    @Mapping(target = "eticket", source = "generarBoletaRequest.visanetResponse.eticket")
    DatosBoletaRPV toDatosBoletaRPV(GenerarBoletaRequest generarBoletaRequest);
}
