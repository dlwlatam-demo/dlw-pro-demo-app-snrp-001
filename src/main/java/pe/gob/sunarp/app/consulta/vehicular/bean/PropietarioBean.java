package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PropietarioBean {
    private String titular;
    private String ts_ult_sync;
}
