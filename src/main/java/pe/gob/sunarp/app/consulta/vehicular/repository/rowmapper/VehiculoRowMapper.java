package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoBean;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoDataBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiculoRowMapper implements RowMapper<VehiculoBean> {

    @Override
    public VehiculoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return VehiculoBean.builder()
                .placa(rs.getString("placa"))
                .partida(rs.getString("partida"))
                .refnumPart(rs.getString("refnumPart"))
                .codLibro(rs.getString("codLibro"))
                .regPubId(rs.getString("regPubId"))
                .oficRegId(rs.getString("oficRegId"))
                .estado(rs.getString("estado"))
                .build();
    }
}
