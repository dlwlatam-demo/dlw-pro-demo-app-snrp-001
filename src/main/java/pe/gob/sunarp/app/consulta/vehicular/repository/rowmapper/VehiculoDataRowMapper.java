package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.VehiculoDataBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class VehiculoDataRowMapper implements RowMapper<VehiculoDataBean> {

    @Override
    public VehiculoDataBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return VehiculoDataBean.builder()
                .no_plac(rs.getString("no_plac"))
                .no_seri(rs.getString("no_seri"))
                .no_vin(rs.getString("no_vin"))
                .no_motr(rs.getString("no_motr"))
                .color(rs.getString("color"))
                .marca(rs.getString("marca"))
                .modelo(rs.getString("modelo"))
                .no_tarj(rs.getString("no_tarj"))
                .titular(rs.getString("titular"))
                .estado(rs.getString("estado"))
                .anotacion(rs.getString("anotacion"))
                .plac_ante((rs.getString("plac_ante") == null || "/".equals(rs.getString("plac_ante")) || "".equals(rs.getString("plac_ante")))? "NINGUNA": rs.getString("plac_ante") )
                .plac_vige((rs.getString("plac_vige") == null || "".equals(rs.getString("plac_vige")) || "/".equals(rs.getString("plac_vige")))?"": rs.getString("plac_vige"))
                .reparticion(rs.getString("reparticion"))
                .ts_ult_sync(rs.getString("ts_ult_sync"))
                .build();
    }
}
