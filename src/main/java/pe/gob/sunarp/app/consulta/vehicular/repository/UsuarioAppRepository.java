package pe.gob.sunarp.app.consulta.vehicular.repository;

import pe.gob.sunarp.app.consulta.vehicular.bean.TaAppsunarpLogBean;
import pe.gob.sunarp.app.consulta.vehicular.bean.UsuarioAppBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;

public interface UsuarioAppRepository {

	void guardarAppLog(TaAppsunarpLogBean appLog)  throws ExceptionBean;
	// UsuarioAppBean getUserByKeyId(String userKeyId) throws ExceptionBean;
}
