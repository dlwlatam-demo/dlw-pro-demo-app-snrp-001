package pe.gob.sunarp.app.consulta.vehicular.service;

import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;

public interface SecurityTokenService {

    void validateToken(String codigo) throws ExceptionBean;
}
