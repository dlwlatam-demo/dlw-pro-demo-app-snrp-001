package pe.gob.sunarp.app.consulta.vehicular.response;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GenerarBoletaResponse implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private long transId;
}
