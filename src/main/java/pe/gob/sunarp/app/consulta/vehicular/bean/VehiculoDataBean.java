package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VehiculoDataBean {
    private String no_plac;
    private String no_seri;
    private String no_vin;
    private String no_motr;
    private String color;
    private String marca;
    private String modelo;
    private String no_tarj;
    private String titular;
    private String estado;
    private String anotacion;
    private String plac_ante;
    private String plac_vige;
    private String reparticion;
    private String ts_ult_sync;
}
