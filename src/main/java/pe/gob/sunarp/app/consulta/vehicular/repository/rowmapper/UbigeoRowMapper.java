package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.UbigeoBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UbigeoRowMapper implements RowMapper<UbigeoBean> {

    @Override
    public UbigeoBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return UbigeoBean.builder()
                .dpto(rs.getString("dpto"))
                .prov(rs.getString("prov"))
                .dist(rs.getString("dist"))
                .build();
    }
}
