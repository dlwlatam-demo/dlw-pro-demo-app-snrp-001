package pe.gob.sunarp.app.consulta.vehicular.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TIVeRequest {
    private String codigoZona;
    private String codigoOficina;
    private String anioTitulo;
    private String numeroTitulo;
    private String numeroPlaca;
    private String codigoVerificacion;
    private String tipo;
}
