package pe.gob.sunarp.app.consulta.vehicular.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuscarTIVeResponse {
    private String documento;
}
