package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ParticipantesPjBean {
    private String razonSocial;
    private String direccion;
    private String anoTitu;
    private String numTitu;
    private String fecProp;
    private String ubigeo;
    private String tipoPers;
    private String tipoDocumento;
    private String numDocumento;
    private String desPart;
}
