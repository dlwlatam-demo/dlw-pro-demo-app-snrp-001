package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.ParticipantesPjBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PropietarioHistoricoPjRowMapper implements RowMapper<ParticipantesPjBean> {

    @Override
    public ParticipantesPjBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        return ParticipantesPjBean.builder()
                .razonSocial(rs.getString("razonSocial"))
                .direccion(rs.getString("direccion"))
                .tipoPers(rs.getString("tipoPers"))
                .tipoDocumento(rs.getString("nombreAbrev"))
                .numDocumento(rs.getString("nuDocIden"))
                .build();
    }
}