package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OficinaRegistralBean {
    public String RegPubId;
    public String OficRegId;
    public String Nombre;

}
