package pe.gob.sunarp.app.consulta.vehicular.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TIVeResponse {
    private String codigoZona;
    private String codigoOficina;
    private String anioTitulo;
    private String numeroTitulo;
    private String numeroPlaca;
    private String codigoVerificacion;
    private String oficina;
    private String tipo;
}
