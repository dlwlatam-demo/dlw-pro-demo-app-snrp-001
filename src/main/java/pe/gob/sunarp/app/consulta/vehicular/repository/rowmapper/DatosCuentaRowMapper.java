package pe.gob.sunarp.app.consulta.vehicular.repository.rowmapper;

import org.springframework.jdbc.core.RowMapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.CuentaBean;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DatosCuentaRowMapper implements RowMapper<CuentaBean> {

    @Override
    public CuentaBean mapRow(ResultSet rs, int rowNum) throws SQLException {
        CuentaBean item = new CuentaBean();
        item.setCuentaId(rs.getLong("cuentaId"));
        item.setLineaPrepagoId(rs.getLong("lineaPrepagoId"));
        return item;
    }
}