package pe.gob.sunarp.app.consulta.vehicular.restClient.impl;

import lombok.extern.slf4j.Slf4j;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionProperties;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeInternoRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeInternoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeInternoValidationResponse;
import pe.gob.sunarp.app.consulta.vehicular.restClient.TIVeRestInternoClient;

import jakarta.annotation.PostConstruct;

import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.GENERIC_SUCCESS;

@Slf4j
@Component
public class TIVeRestInternoClientImpl implements TIVeRestInternoClient {

    @Autowired
    private ApplicationContext context;

    @Autowired
    private ExceptionProperties exceptionProperties;

    HttpHeaders headers = null;
    ObjectMapper objectMapper = null;


    @PostConstruct
    public void init() {
        headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        objectMapper = new ObjectMapper();
    }

    @Override
    public TIVeInternoResponse processTIVeInterno(TIVeInternoRequest tiveRequest) throws ExceptionBean {
        TIVeInternoResponse tiveInternoResponse = null;
        String jsonRequest = null;
        String result = "";
        String url = "";
        String urlValidation = "";

        log.info("TIVeRestInternoClient => processTIVeInterno -> año : " + tiveRequest.getAnioTitulo());
        log.info("TIVeRestInternoClient => processTIVeInterno -> user: " + tiveRequest.getUser());
        TIVeInternoValidationResponse tiveValidation = null;
        RestTemplate restTemplate = new RestTemplate();
        try {
            urlValidation = context.getEnvironment().getProperty("tive.validation.url");

            //----Verification code validation----
            if(tiveRequest.getUser().equals("app")) { //Titulos
                urlValidation+="/999";
            }
            else if(tiveRequest.getUser().equals("publicidad")) { //Publicidades
                urlValidation+="/189";
            }

            urlValidation+="-" + tiveRequest.getAnioTitulo()
                    + "-" + tiveRequest.getNumeroTitulo()
                    + "-" + tiveRequest.getCodigoVerificacion()
                    + "-" + tiveRequest.getCodigoZona()
                    + "-" + tiveRequest.getCodigoOficina()
                    + "-0";

            log.info("TIVeRestInternoClient => validation -> complete validation url: " + urlValidation);

            tiveValidation = restTemplate
                    .getForObject(urlValidation, TIVeInternoValidationResponse.class);

            log.info("TIVeRestInternoClient => validation -> codigoRespuesta: " + tiveValidation.getCodigoRespuesta());
            log.info("TIVeRestInternoClient => validation -> descripcion: " + tiveValidation.getDescripcionRespuesta());
        } catch (Exception e) {
            log.error("TIVeRestInternoClient => processTIVeInterno -> Exception : "
                    + e.getMessage());
            throw exceptionProperties.getErrorGettingDoc();
        }
        
        if(!(tiveValidation.getCodigoRespuesta().equals(GENERIC_SUCCESS))) {
            TIVeInternoResponse tiveInternoResponse1 = new TIVeInternoResponse();
            tiveInternoResponse1.setCode("003");
            tiveInternoResponse1.setErrorMessage("No existe coincidencia, por favor verifique los datos ingresados.");
            log.info("mensaje TIVE: " + tiveValidation.getCodigoRespuesta() + " -- " + tiveValidation.getDescripcionRespuesta());
            return tiveInternoResponse1;
        }
        try {
        	//------------------------------------
            url = context.getEnvironment().getProperty("tive.url");
            log.info("TIVeRestInternoClient => processTIVeInterno -> url : " + url);

            jsonRequest = objectMapper.writeValueAsString(tiveRequest);
            log.info("jsonRequest = " + jsonRequest);
            HttpEntity<String> entity = new HttpEntity<String>(jsonRequest, headers);

            result = restTemplate.postForObject(url, entity, String.class);

            tiveInternoResponse = objectMapper.readValue(result, TIVeInternoResponse.class);
            log.info("tiveInternoResponse: " + tiveInternoResponse.getCode());
            log.info("tiveInternoResponse: " + tiveInternoResponse.getErrorMessage());
	    } catch (Exception e) {
	        log.error("TIVeRestInternoClient => processTIVeInterno -> Exception : "
	                + e.getMessage());
	        throw exceptionProperties.getErrorGettingDoc();
	    }
        return tiveInternoResponse;
    }
}
