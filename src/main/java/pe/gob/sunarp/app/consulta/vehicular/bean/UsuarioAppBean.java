package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
public class UsuarioAppBean {
    private String tipo;
    private Long idUser;
    private String email;
    private String sexo;
    private String tipoDoc;
    private String nroDoc;
    private String nombres;
    private String priApe;
    private String segApe;
    private Date fecNac;
    private String nroCelular;
    private String status;
    private String password;
    private String rememberToken;
    private Date createdAt;
    private Date updatedAt;
    private Date deletedAt;
    private String appVersion;
    private String ipAddress;
    private String codValidacion;
    private String idUsuaCrea;
    private String idUsuaModi;
    private Date lastConn;
    private Double geoLat;
    private Double geoLong;
    private String userKeyId;
    private byte[] userPhoto;
}
