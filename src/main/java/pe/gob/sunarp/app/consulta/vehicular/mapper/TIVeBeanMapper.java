package pe.gob.sunarp.app.consulta.vehicular.mapper;

import org.mapstruct.Mapper;
import pe.gob.sunarp.app.consulta.vehicular.bean.TIVeBean;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeResponse;

@Mapper(componentModel = "spring" )
public interface TIVeBeanMapper {

    TIVeResponse toTIVeResponse(TIVeBean tiveBean);
}
