package pe.gob.sunarp.app.consulta.vehicular.bean;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class VehiculoPartidaBean {
    private String numPlaca;
    private String codModelo;
    private String descModelo;
    private String codMarca;
    private String descMarca;
    private String codCondVeh;
    private String descCondVeh;
    private String codTipoVeh;
    private String descTipoVeh;
    private String codTipoComb;
    private String descTipoComb;
    private String codColorOrig;
    private String color01;
    private String color02;
    private String color03;
    private String anoFab;
    private String numSerie;
    private String numMotor;
    private String numCilindros;
    private String pesoSeco;
    private String pesoBruto;
    private String numPasajeros;
    private String numAsientos;
    private String numEjes;
    private String numRuedas;
    private String numPuertas;
    private String longitud;
    private String ancho;
    private String altura;
    private String tsInscrip;
    private String codTipoCarr;
    private String descTipoCarr;
    private String fgBaja;
    private String refNumPart;
    private String numPartida;
    private String descCetico;
    private String nomOficina;
    private String oficRegId;
    private String areaRegId;
    private String regPubId;
    private String noVin;
    private String coCateg;
    private String anMode;
    private String noVers;
    private String poMotr;
    private String noClda;
    private String noForm;
    private String coTipoUsos;
}
