package pe.gob.sunarp.app.consulta.vehicular.service.impl;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pe.gob.sunarp.app.consulta.vehicular.bean.TIVeBean;
import pe.gob.sunarp.app.consulta.vehicular.cache.RedisService;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionBean;
import pe.gob.sunarp.app.consulta.vehicular.exception.ExceptionProperties;
import pe.gob.sunarp.app.consulta.vehicular.mapper.TIVeBeanMapper;
import pe.gob.sunarp.app.consulta.vehicular.repository.TIVeRepository;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeInternoRequest;
import pe.gob.sunarp.app.consulta.vehicular.request.TIVeRequest;
import pe.gob.sunarp.app.consulta.vehicular.response.BuscarTIVeResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.ProcesoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeInternoResponse;
import pe.gob.sunarp.app.consulta.vehicular.response.TIVeResponse;
import pe.gob.sunarp.app.consulta.vehicular.restClient.TIVeRestInternoClient;
import pe.gob.sunarp.app.consulta.vehicular.service.ConsultaTiveService;
import pe.gob.sunarp.app.seguridad.bean.UsuarioJtiBean;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.BLANK;
import static pe.gob.sunarp.app.consulta.vehicular.util.Constantes.GENERIC_SUCCESS;

@Service
public class ConsultaTiveServiceImpl implements ConsultaTiveService {

	private static final Logger LOG = LoggerFactory.getLogger(ConsultaTiveServiceImpl.class);
	
    @Autowired
    private RedisService redisService;

    @Autowired
    private ExceptionProperties exceptionProperties;

    @Autowired
    private TIVeRestInternoClient tiveRestInterno;

    @Autowired
    private TIVeRepository tiveRepository;

    @Autowired
    private TIVeBeanMapper tiveBeanMapper;

    @Override
    public BuscarTIVeResponse buscarTive(TIVeRequest tiveRequest) throws ExceptionBean {
        BuscarTIVeResponse response = new BuscarTIVeResponse();
        tiveRequest.setNumeroPlaca(tiveRequest.getNumeroPlaca().toUpperCase().trim());
        TIVeInternoResponse tiveInterno = tiveRestInterno.processTIVeInterno(objInRestWithDocument(tiveRequest));
        if(tiveInterno.getCode().equals(GENERIC_SUCCESS)){
            response.setDocumento(tiveInterno.getDocumento());
        }
        else {
    		ExceptionBean exc = exceptionProperties.getErrorGettingDoc();
            String msg = tiveInterno.getErrorMessage();
            if(msg.contains("java")) {
            	msg = "Error: No se pudo obtener TIVe.";
            }
            exc.setDescription(msg);
            throw exc;
        }
        return response;
    }

    @Override
    public List<TIVeResponse> buscarHistorial(String jti) throws ExceptionBean {
        UsuarioJtiBean usuario = redisService.getUsuarioJti(jti);
        List<TIVeBean> tiveList = tiveRepository.getTIVeUserHistory(usuario.getIdUser());
        return tiveList
                .stream()
                .map(tiveBeanMapper::toTIVeResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ProcesoResponse insertarHistorial(String jti, TIVeRequest tiveRequest) throws ExceptionBean {
        ProcesoResponse response = new ProcesoResponse();
        UsuarioJtiBean usuario = redisService.getUsuarioJti(jti);
        tiveRequest.setNumeroPlaca(tiveRequest.getNumeroPlaca().toUpperCase().trim());
        if(this.validarRequest(usuario.getIdUser(), tiveRequest)){
            int countHistory = tiveRepository.countTIVeHistory(usuario.getIdUser(), tiveRequest);

            if(countHistory > 0){
                throw exceptionProperties.getAlreadySavedTIVeHistory();
            }

            tiveRepository.saveTIVeHistory(usuario.getIdUser(), tiveRequest);

            response.setCodResult("1");
            response.setMsgResult("Exito");
        } else {
            throw exceptionProperties.getErrorMissingRequest();
        }
        return response;
    }

    private boolean validarRequest(Long idUser, TIVeRequest tiveRequest) {
        boolean valid = true;
        if (idUser <= 0){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getCodigoZona()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getCodigoOficina()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getAnioTitulo()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getNumeroTitulo()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getNumeroPlaca()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getCodigoVerificacion()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        if (Optional.ofNullable(tiveRequest.getTipo()).orElse(BLANK).equals(BLANK)){
            valid = false;
        }
        return valid;
    }

    private TIVeInternoRequest objInRestWithDocument(TIVeRequest r){
        return new TIVeInternoRequest(
                r.getCodigoZona(),
                r.getCodigoOficina(),
                r.getAnioTitulo(),
                r.getNumeroTitulo(),
                r.getNumeroPlaca(),
                r.getCodigoVerificacion(),
                r.getTipo()
        );
    }


}
