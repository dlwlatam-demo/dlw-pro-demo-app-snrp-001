package pe.gob.sunarp.app.consulta.vehicular.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GetDatosVehiculoRequest {
    private String nuPlac;
    private String codigoRegi;
    private String codigoSede;
    private String ipAddress;
//    private String idUser;
    private String appVersion;
//    private String userKeyId;
//    private String userSessionId;
}
